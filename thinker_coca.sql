-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 01, 2020 at 02:04 PM
-- Server version: 5.1.73
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `thinker_coca`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branchID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branchID`, `title`, `image`, `createDate`, `createBy`, `updateDate`, `updateBy`) VALUES
(1, 'COCA', '3b94e-manuals1.png', NULL, NULL, '2020-03-23 09:52:52', '1'),
(2, 'Mango tree', 'e7d5b5a79a1476d9747f4a714d59ad75.png', NULL, NULL, '2020-03-18 08:14:16', '1'),
(3, 'Mango chili', '1c242-manuals3.png', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `countryID` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`countryID`, `title`, `createDate`, `createBy`, `updateDate`, `updateBy`) VALUES
(3, 'Thailand', '2020-01-23 08:43:33', '1', '2020-03-31 17:14:39', '26'),
(5, 'Malaysia', '2020-01-24 09:22:32', NULL, '2020-03-30 15:47:17', '26'),
(6, 'China', '2020-02-05 05:46:06', '1', '2020-03-31 17:14:46', '26'),
(7, 'Singapore', '2020-03-27 18:23:29', '26', NULL, NULL),
(8, 'Japan', '2020-03-30 15:47:50', '26', NULL, NULL),
(9, 'Hong Kong', '2020-03-30 15:49:06', '26', NULL, NULL),
(10, 'Philippines', '2020-03-30 15:49:23', '26', NULL, NULL),
(11, 'Vietnam ', '2020-03-30 15:49:39', '26', NULL, NULL),
(12, 'Indonesia', '2020-03-30 15:49:59', '26', NULL, NULL),
(13, 'England', '2020-03-30 15:50:21', '26', NULL, NULL),
(14, 'United Arab Emirates', '2020-03-30 15:50:34', '26', NULL, NULL),
(15, 'India', '2020-03-30 15:51:29', '26', NULL, NULL),
(16, 'Nepal', '2020-03-30 15:51:42', '26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country_zone`
--

CREATE TABLE `country_zone` (
  `zoneID` int(11) NOT NULL,
  `countryID` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country_zone`
--

INSERT INTO `country_zone` (`zoneID`, `countryID`, `title`, `createDate`, `createBy`, `updateDate`, `updateBy`) VALUES
(3, 3, 'Surawongse', '2020-02-05 04:55:10', '1', '2020-03-30 15:52:41', '26'),
(4, 3, 'Siam Square', '2020-02-05 04:55:16', '1', '2020-03-30 15:52:51', '26'),
(6, 6, 'Qingtao', '2020-02-05 05:46:21', '1', '2020-03-30 16:43:26', '26'),
(7, 5, 'KL', '2020-02-05 08:19:05', '1', '2020-03-31 18:11:42', '26'),
(9, 7, 'Ngee Ann City', '2020-03-27 18:23:45', '26', NULL, NULL),
(10, 7, 'Leisure Park Kallang', '2020-03-27 18:23:57', '26', NULL, NULL),
(11, 7, 'Suntec City ', '2020-03-27 18:24:07', '26', NULL, NULL),
(12, 3, 'Sukhumvit 39', '2020-03-30 15:54:12', '26', NULL, NULL),
(13, 8, 'Tokyo', '2020-03-30 15:56:24', '26', NULL, NULL),
(14, 8, 'Hakata', '2020-03-30 15:56:42', '26', '2020-03-30 16:39:54', '26'),
(15, 8, 'Shinjuku ', '2020-03-30 15:56:58', '26', NULL, NULL),
(16, 8, 'Yokohama', '2020-03-30 16:23:25', '26', NULL, NULL),
(17, 8, 'Ebisu', '2020-03-30 16:23:35', '26', NULL, NULL),
(18, 8, 'Ikebukuro', '2020-03-30 16:23:49', '26', NULL, NULL),
(19, 8, 'Osaka', '2020-03-30 16:24:05', '26', NULL, NULL),
(20, 8, 'Toyosu', '2020-03-30 16:35:30', '26', NULL, NULL),
(21, 8, 'Shinagawa', '2020-03-30 16:35:47', '26', NULL, NULL),
(22, 8, 'Omiya', '2020-03-30 16:35:57', '26', NULL, NULL),
(23, 8, 'Ueno', '2020-03-30 16:36:11', '26', NULL, NULL),
(24, 8, 'EXPOCITY', '2020-03-30 16:36:51', '26', NULL, NULL),
(25, 8, 'Kawasaki', '2020-03-30 16:37:08', '26', NULL, NULL),
(26, 8, 'Nagoya', '2020-03-30 16:37:19', '26', NULL, NULL),
(27, 8, 'Tenjin', '2020-03-30 16:37:29', '26', NULL, NULL),
(28, 8, 'Kitasenju', '2020-03-30 16:37:39', '26', NULL, NULL),
(31, 8, 'GRANSTA', '2020-03-30 16:39:31', '26', NULL, NULL),
(33, 8, 'Shibuya', '2020-03-30 16:41:00', '26', NULL, NULL),
(34, 8, 'Omiya', '2020-03-30 16:41:28', '26', NULL, NULL),
(35, 8, 'Hiroshima', '2020-03-30 16:41:53', '26', NULL, NULL),
(36, 8, 'Shinsaibashi', '2020-03-30 16:42:07', '26', NULL, NULL),
(37, 8, 'Kinshicyo', '2020-03-30 16:42:18', '26', NULL, NULL),
(38, 6, 'Guangzhou', '2020-03-30 16:43:42', '26', NULL, NULL),
(39, 10, 'Manila', '2020-03-30 16:49:44', '26', NULL, NULL),
(40, 10, 'Baguio', '2020-03-30 16:49:54', '26', '2020-03-30 16:50:49', '26'),
(41, 11, 'Ho Chi Minh', '2020-03-30 17:43:14', '26', NULL, NULL),
(42, 11, 'Hanoi ', '2020-03-30 17:43:57', '26', NULL, NULL),
(43, 12, 'Bandung', '2020-03-30 17:44:14', '26', NULL, NULL),
(44, 12, 'Graha Mas', '2020-03-30 17:44:23', '26', NULL, NULL),
(45, 12, 'Kemang ', '2020-03-30 17:45:10', '26', '2020-03-30 17:45:21', '26'),
(46, 13, ' London', '2020-03-30 17:45:41', '26', NULL, NULL),
(47, 16, 'Kathmandu', '2020-03-30 17:46:02', '26', NULL, NULL),
(48, 15, 'Bangalore', '2020-03-30 17:46:17', '26', NULL, NULL),
(49, 7, 'Changi', '2020-03-30 17:47:08', '26', NULL, NULL),
(50, 3, 'Time Square', '2020-03-31 18:01:31', '26', NULL, NULL),
(51, 3, 'Central World', '2020-03-31 18:01:40', '26', NULL, NULL),
(52, 3, 'Central Chaengwattana', '2020-03-31 18:01:50', '26', NULL, NULL),
(53, 3, 'Mega Bangna', '2020-03-31 18:02:00', '26', NULL, NULL),
(54, 3, 'Hua Hin', '2020-03-31 18:02:09', '26', NULL, NULL),
(55, 3, 'On The River', '2020-03-31 18:02:24', '26', NULL, NULL),
(58, 8, 'Pad Thai', '2020-03-31 18:05:53', '26', NULL, NULL),
(59, 9, 'Elements', '2020-03-31 18:06:45', '26', '2020-03-31 18:07:03', '26'),
(60, 9, 'Cityplaza', '2020-03-31 18:06:58', '26', NULL, NULL),
(61, 9, 'Yoho', '2020-03-31 18:07:12', '26', NULL, NULL),
(62, 10, 'Trinoma', '2020-03-31 18:07:56', '26', '2020-03-31 18:09:08', '26'),
(63, 10, 'Greenbelt', '2020-03-31 18:08:08', '26', '2020-03-31 18:09:13', '26'),
(64, 10, 'Manila Bay ', '2020-03-31 18:09:27', '26', NULL, NULL),
(65, 10, 'Baguio', '2020-03-31 18:09:37', '26', NULL, NULL),
(66, 14, 'JBR', '2020-03-31 18:12:04', '26', NULL, NULL),
(67, 3, 'Suvarnbumi Airport', '2020-03-31 19:14:37', '26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `franchisee`
--

CREATE TABLE `franchisee` (
  `franchiseeID` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `franchisee`
--

INSERT INTO `franchisee` (`franchiseeID`, `title`, `createDate`, `createBy`, `updateDate`, `updateBy`) VALUES
(1, 'Singapore', '2020-02-19 10:05:25', NULL, '2020-03-31 17:30:09', '26'),
(4, 'Hong Kong + China', '2020-03-30 15:06:28', '26', '2020-03-30 15:38:23', '26'),
(7, 'Japan', '2020-03-30 15:07:05', '26', NULL, NULL),
(9, 'Philippines', '2020-03-30 15:28:54', '26', NULL, NULL),
(11, 'Thailand', '2020-03-30 15:29:27', '26', NULL, NULL),
(12, ' Vietnam (HCMC)', '2020-03-30 15:29:42', '26', '2020-03-30 15:36:44', '26'),
(13, 'Vietnam (Hanoi)', '2020-03-30 15:37:04', '26', NULL, NULL),
(14, 'Indonesia', '2020-03-31 18:13:41', '26', NULL, NULL),
(15, 'Malaysia', '2020-03-31 18:13:50', '26', NULL, NULL),
(16, 'Thailand (Airport)', '2020-03-31 19:12:13', '26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menuID` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `label` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menuID`, `sort`, `label`, `icon`, `url`) VALUES
(1, 2, 'F&B / Operations', 'notes', '#'),
(2, 1, 'Marketing', 'notes', '#'),
(3, 50, 'Training', 'group', '#'),
(4, 50, 'Report', 'settings', '#'),
(5, 3, 'Resources', 'notes', '#'),
(10, 0, 'News & Alerts', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `menu_sub`
--

CREATE TABLE `menu_sub` (
  `subID` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `label` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(150) NOT NULL,
  `menuID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_sub`
--

INSERT INTO `menu_sub` (`subID`, `sort`, `label`, `icon`, `url`, `menuID`) VALUES
(1, 1, 'Recipe Center', 'lock', 'admin/product_category', 1),
(2, 1, 'Orientation ', '', 'admin/users', 3),
(3, 2, 'Brand Overview', '', 'admin/groups', 3),
(4, 3, 'Management Training', '', 'admin/menu', 3),
(5, 1, 'Marketing Toolkit', '', 'admin/about_category', 2),
(6, 2, 'Branding Toolkit', '', 'admin/about_detail', 2),
(9, 2, 'Manuals & Form', '', 'admin/product', 1),
(10, 1, 'Images', '', 'admin/news_category', 5),
(12, 1, 'Corporate Audit (corporate)', '', 'admin/settings', 4),
(13, 2, 'Monthly Audit', '', 'admin/seo', 4),
(17, 2, 'Menu', '', 'admin/news', 5),
(22, 0, 'Daily checklist', '', '', 4),
(23, 0, 'Promotions', '', '', 5),
(24, 0, 'Food & Beverage', '', '', 3),
(25, 0, 'Service', '', '', 3),
(26, 0, 'Monthly report', '', '', 4);

-- --------------------------------------------------------

--
-- Table structure for table `operations_manuals`
--

CREATE TABLE `operations_manuals` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `files` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operations_manuals`
--

INSERT INTO `operations_manuals` (`id`, `category_id`, `title`, `content`, `files`) VALUES
(1, 1, 'Checklist for Internal Audit', '', 'cef39-01-checklist-for-internal-audit.xls'),
(2, 1, 'Chef Kitchen Staff Duties', '', 'f166a-01-chef-_-kitchen-staff-duties.pdf'),
(3, 1, 'Cleaning Standards', '', 'f2339-01-cleaning-standards.docx'),
(4, 1, 'Cleanliness and Hygienic Conditions', '', '9a361-01-cleanliness-and-hygienic-conditions.doc'),
(5, 1, 'COCA Crockery, Cutlery, Glassware and Utensils', '', '38e85-01-coca-crockery-cutlery-glassware-and-utensils.xls'),
(6, 1, 'Coca Foods Production Plan', '', '69bef-01-coca-foods-production-plan.pdf'),
(7, 1, 'Coca Learning Kit (Ingredient)', '', '505ac-01-coca-learning-kit-ingredient-.pdf'),
(8, 1, 'Daily Kitchen Preparation Lists (sample from Mango Tree)', '', '47b1c-01-daily-kitchen-preparation-lists-sample-from-mango-tree-.xls'),
(9, 1, 'Dishwasher Duties', '', '07a1d-01-dishwasher-duties.docx'),
(10, 1, 'FM-KC-01.01(06) - Kitchen Opening', '', '2b6ea-01-fm-kc-01.01-06-kitchen-opening.xls'),
(11, 2, 'Bartender (cover)', '', 'da7bc-00-bartender-cover-.doc'),
(12, 2, 'Beer Stock Controls Template (cover)', '', '5c40d-00-beer-stock-controls-template-cover-.doc'),
(13, 2, 'Cash, Financial Management Control (cover)', '', '3018a-00-cash-financial-management-_-control-cover-.doc'),
(14, 2, 'Cashier (cover)', '', 'b56c0-00-cashier-cover-.doc'),
(15, 2, 'Chef Kitchen Staff (cover)', '', '90845-00-chef-_-kitchen-staff-cover-.doc'),
(16, 2, 'Cigarette Stock Controls Template (cover)', '', 'a4f4a-00-cigarette-stock-controls-template-cover-.doc'),
(17, 2, 'Cleanliness and Hygienic Conditions (cover)', '', 'b272d-00-cleanliness-and-hygienic-conditions-cover-.doc'),
(18, 2, 'Daily Kitchen Check Lists (Cover)', '', '41c60-00-daily-kitchen-check-lists-cover-.doc'),
(19, 2, 'Daily Kitchen Preparation Lists (Cover)', '', '21739-00-daily-kitchen-preparation-lists-cover-.doc'),
(20, 2, 'Dining Room Management Procedure (cover)', '', 'ee7bd-00-dining-room-management-_-procedure-cover-.doc');

-- --------------------------------------------------------

--
-- Table structure for table `resources_images`
--

CREATE TABLE `resources_images` (
  `imagesID` int(11) NOT NULL,
  `files` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `countryID` int(11) NOT NULL,
  `zoneID` int(11) NOT NULL,
  `branchID` int(11) NOT NULL,
  `restaurantID` int(11) NOT NULL,
  `createDate` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=Active, 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `resources_images`
--

INSERT INTO `resources_images` (`imagesID`, `files`, `countryID`, `zoneID`, `branchID`, `restaurantID`, `createDate`, `status`) VALUES
(19, 'c1eab6ecdb1bf1d0b12871587bff886d.jpg', 3, 3, 1, 4, '2020-03-31 12:21:21', '1');

-- --------------------------------------------------------

--
-- Table structure for table `resources_images_category`
--

CREATE TABLE `resources_images_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resources_images_category`
--

INSERT INTO `resources_images_category` (`id`, `title`, `image`) VALUES
(1, 'COCA', '3b94e-manuals1.png'),
(2, 'Mango tree', '3c6f3-manuals2.png'),
(3, 'Mango chili', '1c242-manuals3.png');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant`
--

CREATE TABLE `restaurant` (
  `restaurantID` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `detail` text,
  `branchID` int(11) NOT NULL,
  `franchiseeID` int(11) NOT NULL,
  `countryID` int(11) DEFAULT NULL,
  `zoneID` int(11) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `restaurant`
--

INSERT INTO `restaurant` (`restaurantID`, `title`, `detail`, `branchID`, `franchiseeID`, `countryID`, `zoneID`, `createDate`, `createBy`, `updateDate`, `updateBy`) VALUES
(4, 'Coca Surawongse', '', 1, 11, 3, 3, '2020-03-31 18:30:26', '26', NULL, NULL),
(5, 'Coca Siam Square', '', 1, 11, 3, 4, '2020-03-31 18:30:47', '26', NULL, NULL),
(6, 'Coca Sukhumvit 39', '', 1, 11, 3, 12, '2020-03-31 18:31:45', '26', NULL, NULL),
(7, 'Coca Time Square', '', 1, 11, 3, 50, '2020-03-31 18:33:03', '26', NULL, NULL),
(8, 'Coca Central World', '', 1, 11, 3, 51, '2020-03-31 18:33:24', '26', NULL, NULL),
(9, 'COCA Central Chaengwattana', '', 1, 11, 3, 52, '2020-03-31 18:33:42', '26', NULL, NULL),
(10, 'Coca Mega Bangna', '', 1, 11, 3, 53, '2020-03-31 18:34:01', '26', NULL, NULL),
(11, 'Coca Hua Hin', '', 1, 11, 3, 54, '2020-03-31 18:34:20', '26', NULL, NULL),
(12, 'Mango Tree Surawongse', '', 2, 11, 3, 3, '2020-03-31 18:34:39', '26', NULL, NULL),
(13, 'Mango Tree On The River', '', 2, 11, 3, 55, '2020-03-31 18:35:02', '26', NULL, NULL),
(14, 'Mango Tree Cafe Central World', '', 2, 11, 3, 51, '2020-03-31 18:35:24', '26', '2020-03-31 19:16:55', '26'),
(15, 'Mango Tree @SVB Airport ', '', 2, 16, 3, 67, '2020-03-31 19:13:26', '26', '2020-03-31 19:19:34', '26'),
(16, ' Mango Tree Grab & Go (SVB Airport)', '', 2, 16, 3, 67, '2020-03-31 19:19:58', '26', NULL, NULL),
(17, 'Coca Restaurant Ngee Ann City', '', 1, 1, 7, 9, '2020-03-31 19:20:32', '26', NULL, NULL),
(18, 'Coca Restaurant Leisure Park Kallang', '', 1, 1, 7, 10, '2020-03-31 19:20:55', '26', NULL, NULL),
(19, 'Coca Restaurant Suntec City', '', 1, 1, 7, 11, '2020-03-31 19:21:16', '26', NULL, NULL),
(20, '  Mango Tree Tokyo', '', 2, 7, 8, 13, '2020-04-01 11:17:52', '26', '2020-04-01 11:20:42', '26'),
(21, ' Mango Tree Bistro Hakata', '', 2, 7, 8, 14, '2020-04-01 11:20:35', '26', NULL, NULL),
(22, '  Mango Tree Café Shinjuku ', '', 2, 7, 8, 15, '2020-04-01 11:21:11', '26', NULL, NULL),
(23, '  Mango Tree Café Yokohama', '', 2, 7, 8, 16, '2020-04-01 11:23:18', '26', NULL, NULL),
(24, '  Mango Tree Café Ebisu', '', 2, 7, 8, 17, '2020-04-01 11:23:39', '26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `training`
--

CREATE TABLE `training` (
  `id` int(11) NOT NULL,
  `training_group_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `training`
--

INSERT INTO `training` (`id`, `training_group_id`, `title`, `time`) VALUES
(1, 1, 'What are the names of Coca’s founders ?', NULL),
(2, 1, 'When was COCA first established ?', NULL),
(3, 1, 'The name “Coca” came from the word “Ker Co”. What does it mean ?', NULL),
(4, 1, 'Ms. Pattama was originally from China. Where did she develop her cooking skills as a hotel caterer ?', NULL),
(5, 1, 'What type of cuisine does Coca serve?', NULL),
(6, 1, 'What has been Coca’s signature menu?', NULL),
(7, 1, 'The 2nd generation Pitaya studied overseas and expanded Coca in the international market. Where was the first Coca located internationally?', NULL),
(8, 1, 'According to Pitaya’s what is the core of a “Restaurant Business”?', NULL),
(9, 1, 'What is Coca Foods International?', NULL),
(10, 1, 'Coca also introduced a Thai restaurant to its existing folio. What is the name of the Thai restaurant brand?', NULL),
(11, 2, 'What are the concepts of Coca?', NULL),
(12, 2, 'What are the key elements of Coca’s CARE?', NULL),
(13, 2, 'According to Pitaya, what is the basics of a good restaurant?', NULL),
(14, 2, 'Pitaya’s principle of running a good and successful restaurants are the following, EXCEPT:', NULL),
(15, 2, 'What is the strategy used that has been a key to success in expanding the business?', NULL),
(16, 2, 'What is the key to efficiency according to Coca?', NULL),
(17, 2, 'The 3rd generation has introduced Coca Boutique Farm. What are the purpose of the farm?', NULL),
(18, 2, 'The chili from Coca Boutique Farm are mainly used for which item?', NULL),
(19, 3, 'When was Mango Tree first established?', NULL),
(20, 3, 'Apart from great food, what diners are expecting more in today’s world?', NULL),
(21, 3, 'What is the key concept of Mango Tree restaurants?', NULL),
(22, 3, 'Mango Tree restaurants are located in several gateway cities in the world, except:', NULL),
(23, 3, 'How many Mango Tree locations are there internationally?', NULL),
(24, 4, 'Mango Chili is a unique Thai quick service restaurant concept. What type of Thai cuisine inspires the brand?', NULL),
(25, 4, 'Currebtly, there are 2 locations of Mango Chili, except:', NULL),
(26, 4, 'What is the website of Mango Chili?', NULL),
(27, 5, 'How many seats did the first Coca restaurant have?', NULL),
(28, 5, 'What is Natalie Phanphensophon’s specialized field?', NULL),
(29, 5, 'The core mission of Coca are as follow, EXCEPT:', NULL),
(30, 5, 'Where did the name “Mango Tree” come from?', NULL),
(31, 5, 'What are the key flavours of Thai cuisine?', NULL),
(32, 5, 'Which restaurant is the first to receive a Michelin Plate award?', NULL),
(33, 5, 'What Mango tree brand offer the holiday experience while providing local vibes and authentic food?', NULL),
(34, 5, 'What Mango tree brand offer creative Thai twist menu and drinks within a fun, casual environment?', NULL),
(35, 5, 'Mango Tree Kitchen adopted the concept from the street food stalls in Thailand and offers the local favourite dishes. What are the specialized menu for Mango Tree Kitchen?', NULL),
(36, 5, 'Mango Tree Grab & Go offers the following, EXCEPT:', NULL),
(37, 5, 'Who is the Managing Director of Asian Cuisine & Hospitality, the franchising company of COCA Holding International, who is the main force of the brand’s international expansion ?', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `training_answer`
--

CREATE TABLE `training_answer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `training_id` int(11) DEFAULT NULL,
  `training_choice_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `training_answer`
--

INSERT INTO `training_answer` (`id`, `user_id`, `training_id`, `training_choice_id`, `score`) VALUES
(1, 1, 1, 1, 0),
(2, 1, 2, 7, 1),
(3, 1, 3, 13, 0),
(4, 1, 4, 15, 0),
(5, 1, 5, 18, 0),
(6, 1, 6, 21, 0),
(7, 1, 7, 24, 0),
(8, 1, 8, 28, 0),
(9, 1, 9, 32, 0),
(10, 1, 10, 36, 1);

-- --------------------------------------------------------

--
-- Table structure for table `training_choice`
--

CREATE TABLE `training_choice` (
  `id` int(11) NOT NULL,
  `training_id` int(11) DEFAULT NULL,
  `detail` varchar(500) DEFAULT NULL,
  `answer` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `training_choice`
--

INSERT INTO `training_choice` (`id`, `training_id`, `detail`, `answer`) VALUES
(1, 1, '<h4>\n	a. Srichai&nbsp;</h4>\n', 0),
(2, 1, '<p>\n	b. Pattama</p>\n', 0),
(3, 1, '<p>\n	c. Pitaya</p>\n', 0),
(4, 1, '<p>\n	d. Srichai &amp; Pattama</p>\n', 1),
(5, 1, '<p>\n	e. Pitaya &amp; Pattama</p>\n', 0),
(6, 2, '<p>\n	a. The year 1942</p>\n', 0),
(7, 2, '<p>\n	b. The year 1957</p>\n', 1),
(8, 2, '<p>\n	c. The year 1969</p>\n', 0),
(9, 2, '<div>\n	d. The year 1985</div>\n<div>\n	&nbsp;</div>\n', 0),
(10, 3, '<p>\n	a. Original</p>\n', 0),
(11, 3, '<p>\n	b.&nbsp;Hot Pot</p>\n', 0),
(12, 3, '<p>\n	c.&nbsp;Delicious</p>\n', 1),
(13, 3, '<p>\n	d.&nbsp;Happiness</p>\n', 0),
(14, 4, '<p>\n	a.&nbsp;Hong Kong</p>\n', 1),
(15, 4, '<p>\n	b.&nbsp;China</p>\n', 0),
(16, 4, '<p>\n	a.&nbsp;Thailand</p>\n', 0),
(17, 5, '<p>\n	a.&nbsp;Chinese cuisine</p>\n', 0),
(18, 5, '<p>\n	b.&nbsp;Thai cuisine</p>\n', 0),
(19, 5, '<p>\n	c.&nbsp;Cantonese cuisine</p>\n', 1),
(20, 6, '<p>\n	a. Ancient style Chinese Suki</p>\n', 1),
(21, 6, '<p>\n	b. Japanese style Hot Pot</p>\n', 0),
(22, 6, '<p>\n	c.&nbsp;Thai style Hot Soup</p>\n', 0),
(23, 7, '<p>\n	a.&nbsp;Canada</p>\n', 0),
(24, 7, '<p>\n	b.&nbsp;London</p>\n', 0),
(25, 7, '<p>\n	c.&nbsp;Hong Kong</p>\n', 0),
(26, 7, '<p>\n	d.&nbsp;Singapore</p>\n', 1),
(27, 8, 'a. The heart', 1),
(28, 8, 'b. The food', 0),
(29, 8, 'c. The service', 0),
(30, 8, 'd. The atmosphere', 0),
(31, 9, 'a. Main kitchen', 0),
(32, 9, 'b. Franchise company', 0),
(33, 9, 'c. Material production factory', 1),
(34, 9, 'd. Organic farm', 0),
(35, 10, 'a. Nikai', 0),
(36, 10, 'b. Mango Tree', 1),
(37, 10, 'c. China White', 0),
(38, 10, 'd. Botantei', 0),
(39, 11, 'a. Simple, Healthy & Lively', 1),
(40, 11, 'b. Fresh, organic & delicious', 0),
(41, 11, 'c. Care, healthy & delicious', 0),
(42, 12, 'a. Caring, Appetizing, Real, Effectiveness', 0),
(43, 12, 'b. Consistency, Accomplishment, Relations, Efficiency', 1),
(44, 12, 'c. Consistency, Appealing, Recognition, Effectiveness', 0),
(45, 13, 'a. Great and friendly service', 0),
(46, 13, 'b. Cozy and comfortable atmosphere', 0),
(47, 13, 'c. Good food, good ingredients', 1),
(48, 14, 'a. Good food', 0),
(49, 14, 'b. Good service', 0),
(50, 14, 'c. Good ambiance', 0),
(51, 14, 'd. Good system ', 1),
(52, 15, 'a. Global standard and auditing', 0),
(53, 15, 'b. Different tiering of brands', 1),
(54, 15, 'c. Strong staff loyalty', 0),
(55, 16, 'a. Sustainability', 1),
(56, 16, 'b. ISO Standards', 0),
(57, 16, 'c. Quality Training', 0),
(58, 17, 'a. To help support local society', 0),
(59, 17, 'b. To provide fresh and organic ingredients to the restaurants', 0),
(60, 17, 'c. Both of the above', 1),
(61, 18, 'a. Curry Sauce', 0),
(62, 18, 'b. Chili Sauce', 0),
(63, 18, 'c. Sweet Chili Sauce', 0),
(64, 18, 'd. Suki Sauce', 1),
(65, 19, 'a. The year 1957', 0),
(66, 19, 'b. The year 1969', 0),
(67, 19, 'c. The year 1985', 0),
(68, 19, 'd. The year 1994', 1),
(69, 20, 'a. Presentation', 0),
(70, 20, 'b. Exceptional service', 0),
(71, 20, 'c. Recognition', 0),
(72, 20, 'd. Entertainment', 0),
(73, 20, 'e. All of the above', 1),
(74, 21, 'a. Life.Style.Taste', 1),
(75, 21, 'b. Taste & Lifestyle', 0),
(76, 21, 'c. Thai cuisine at its best', 0),
(77, 22, 'a. London', 0),
(78, 22, 'b. Tokyo', 0),
(79, 22, 'c. Dubai', 0),
(80, 22, 'd. Singapore', 1),
(81, 23, 'a. 30 restaurants in 12 countries', 0),
(82, 23, 'b. 40 restaurants in 13 countries', 0),
(83, 23, 'c. 50 restaurants in 14 countries', 1),
(84, 24, 'a. Thai traditional cuisine', 0),
(85, 24, 'b. Vibrant street food of Bangkok', 1),
(86, 24, 'c. Regional Thai food', 0),
(87, 25, 'a. Mumbai, India', 1),
(88, 25, 'b. Bangalore, India', 0),
(89, 25, 'c. Kathmandu, Nepal', 0),
(90, 26, 'a. MangoTree.com', 0),
(91, 26, 'b. MangoChili.com', 1),
(92, 26, 'c. MangoChiliCafe.com', 0),
(93, 27, 'a. 10 seats', 0),
(94, 27, 'b. 20 seats', 1),
(95, 27, 'c. 30 seats', 0),
(96, 27, 'd. 50 seats', 0),
(97, 28, 'a. A renown celebrity chef for traditional Thai cuisine', 0),
(98, 28, 'b. A modern, self-taught sustainable farmer', 0),
(99, 28, 'c. A nutritionist focusing on healthier cooking techniques and products', 1),
(100, 29, 'a. We are the world’s premium Thai restaurant chain', 1),
(101, 29, 'b. We only serve food we would share with our children.', 0),
(102, 29, 'c. We care about the earth like how it has supported our lives.', 0),
(103, 29, 'd. We share our passion for real food and memorable experiences.', 0),
(104, 29, 'd. We are the Coca Community. We live and breathe our recipe every day.', 0),
(105, 30, 'a. The famous Thai dessert “mango & sticky rice”', 0),
(106, 30, 'b. It’s the founder’s favourite fruit', 0),
(107, 30, 'c. The mango tree planted in front of the first restaurant', 1),
(108, 31, 'a. Sweet and spicy', 0),
(109, 31, 'b. Sweet and sour', 0),
(110, 31, 'c. Sweet, salty and spicy', 0),
(111, 31, 'd. Sweet, sour, salty and spicy', 1),
(112, 32, 'a. Mango Tree Hong Kong', 0),
(113, 32, 'b. Mango Tree Guangzhou', 1),
(114, 32, 'c. Coca Bangkok', 0),
(115, 32, 'd. Coca Singapore', 0),
(116, 33, 'a. Mango Tree', 0),
(117, 33, 'b. Mango Tree Thai Bistro', 0),
(118, 33, 'c. Mango Tree Café', 1),
(119, 33, 'd. Mango Tree Kitchen', 0),
(120, 33, 'e. Mango Tree Grab & Go', 0),
(121, 34, 'a. Mango Tree', 0),
(122, 34, 'b. Mango Tree Thai Bistro', 1),
(123, 34, 'c. Mango Tree Café', 0),
(124, 34, 'd. Mango Tree Kitchen', 0),
(125, 34, 'd. Mango Tree Grab & Go', 0),
(126, 35, 'a. Pad Thai', 0),
(127, 35, 'b. Khao Mun Gai', 0),
(128, 35, 'c. Gapao', 0),
(129, 35, 'd. All of the above', 1),
(130, 36, 'a. Thai food only', 1),
(131, 36, 'b. Counter service only', 0),
(132, 36, 'c. Takeaway food items only', 0),
(133, 36, 'd. A wide selection of takeaway beverage menu ', 0),
(134, 37, 'a. Pitaya Phanphensophon', 0),
(135, 37, 'b. Trevor MacKenzie', 1),
(136, 37, 'c. Natalie Phanphensophon', 0);

-- --------------------------------------------------------

--
-- Table structure for table `training_group`
--

CREATE TABLE `training_group` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `groups` varchar(255) DEFAULT NULL,
  `video` text,
  `detail` text,
  `total` int(11) DEFAULT NULL,
  `must` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `training_group`
--

INSERT INTO `training_group` (`id`, `title`, `groups`, `video`, `detail`, `total`, `must`) VALUES
(1, 'COCA 50th Year Anniversary', 'Orientation', '3981f-1.-coca-50th-year-anniversary_low.mp4', NULL, 10, 8),
(4, 'Mango Chili Thai Cafe', 'Orientation', 'b6a5f-4.-mango-chili-thai-cafe.mp4', NULL, 3, 2),
(5, 'Coca & Mango Tree Restaurants Group (2020)', 'Brand Overview', NULL, NULL, 11, 8),
(2, 'COCA 60th Year Anniversary ', 'Orientation', '84ca0-2.-coca-60th-year-anniversary-.mp4', NULL, 8, 5),
(3, 'Mango Tree Restaurants_low', 'Orientation', '0a334-3.-mango-tree-restaurants_low.mp4', NULL, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `usersID` int(11) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `email` varchar(100) NOT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `typeID` int(11) DEFAULT NULL,
  `groupID` int(11) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usersID`, `username`, `password`, `email`, `active`, `fullname`, `phone`, `type`, `typeID`, `groupID`, `createDate`, `createBy`, `updateDate`, `updateBy`) VALUES
(26, 'super_admin', 'MTIzNA==', 'super_admin@gmail.com', 1, 'superadmin coca', NULL, 'corporate', 7, 1, '2020-03-24 04:46:59', '25', NULL, NULL),
(27, 'Lewis', 'MTIzNA==', 'lewis@coca.com.sg', 1, 'Lewis Tan', NULL, 'Franchisee', 1, 18, '2020-03-27 04:42:43', '26', '2020-03-27 15:41:28', '26'),
(28, 'Adelfa', 'MTIzNA==', 'adelfa@ssp.com.sg', 1, 'Adelfa Mendoza', NULL, 'Restaurant', 1, 14, '2020-03-27 15:39:12', '26', NULL, NULL),
(29, 'Melvin', '4LmFLy3guKA=', 'melvin@coca.com.sg', 1, 'Melvin Ng', NULL, 'Franchisee', 1, 19, '2020-03-27 18:21:39', '26', NULL, NULL),
(30, 'thinker', 'MTIzNA==', 'paul@thinker.com', 1, 'phokai', NULL, 'Restaurant', 3, 14, '2020-03-27 18:54:11', '26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_access`
--

CREATE TABLE `users_access` (
  `accessID` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_access`
--

INSERT INTO `users_access` (`accessID`, `title`, `level`, `createDate`, `createBy`, `updateDate`, `updateBy`) VALUES
(1, 'Corporate', 0, '2020-03-19 04:44:40', '1', NULL, NULL),
(2, 'Franchisee', 0, '2020-03-19 04:48:21', '1', NULL, NULL),
(3, 'Restaurant', 0, '2020-03-19 04:48:29', '1', NULL, NULL),
(12, 'Asian Cuisine & Hospitality', 1, '2020-03-27 07:21:20', '26', NULL, NULL),
(13, 'COCA Holding International', 1, '2020-03-27 07:21:46', '26', '2020-03-27 18:14:35', '26');

-- --------------------------------------------------------

--
-- Table structure for table `users_group`
--

CREATE TABLE `users_group` (
  `groupID` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `accessID` int(11) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `createBy` varchar(50) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `updateBy` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_group`
--

INSERT INTO `users_group` (`groupID`, `title`, `accessID`, `createDate`, `createBy`, `updateDate`, `updateBy`) VALUES
(1, 'Super admin', 12, '2020-03-23 12:00:25', '1', NULL, NULL),
(2, 'Admin', 12, '2020-03-23 12:00:31', '1', NULL, NULL),
(3, 'Management', 12, '2020-03-23 12:04:51', '1', NULL, NULL),
(4, 'Operation/FB', 12, '2020-03-23 12:05:17', '1', NULL, NULL),
(5, 'Chef', 12, '2020-03-23 12:05:29', '1', NULL, NULL),
(6, 'Marketing', 12, '2020-03-23 12:05:50', '1', NULL, NULL),
(7, 'Staff', 12, '2020-03-23 12:06:00', '1', NULL, NULL),
(8, 'Admin', 13, '2020-03-23 12:06:35', '1', NULL, NULL),
(9, 'Management', 13, '2020-03-23 12:06:41', '1', NULL, NULL),
(10, 'Operation/FB', 13, '2020-03-23 12:06:50', '1', NULL, NULL),
(11, 'Marketing', 13, '2020-03-24 03:46:48', NULL, '2020-03-24 03:54:27', '25'),
(14, 'Restaurant Manager', 3, '2020-03-27 07:29:45', '26', '2020-03-27 15:33:29', '1'),
(15, 'Management', 3, '2020-03-27 07:30:07', '26', '2020-03-27 15:33:41', '1'),
(16, 'Ops/ FB', 3, '2020-03-27 15:33:49', '1', '2020-03-27 18:51:38', '1'),
(17, 'Chef', 3, '2020-03-27 15:34:02', '1', NULL, NULL),
(18, 'Admin', 2, '2020-03-27 15:34:46', '1', '2020-03-27 15:35:07', '1'),
(19, 'Management', 2, '2020-03-27 15:34:51', '1', '2020-03-27 15:34:57', '1'),
(20, 'Ops/ FB', 2, '2020-03-27 15:35:02', '1', '2020-03-27 18:59:47', '1'),
(21, 'Marketing', 3, '2020-03-27 18:51:48', '1', NULL, NULL),
(22, 'HR/ Staff/ Accounting', 3, '2020-03-27 18:52:03', '1', NULL, NULL),
(23, 'Chef', 2, '2020-03-27 19:00:04', '1', NULL, NULL),
(24, 'Marketing', 2, '2020-03-27 19:00:11', '1', NULL, NULL),
(25, 'HR/ Staff/ Accounting', 2, '2020-03-27 19:00:20', '1', NULL, NULL),
(26, 'HR/ Staff/ Accounting', 13, '2020-03-30 15:02:46', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_permission_add`
--

CREATE TABLE `users_permission_add` (
  `addID` int(11) NOT NULL,
  `groupID` int(11) DEFAULT NULL,
  `menuID` int(11) DEFAULT NULL,
  `subID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_permission_add`
--

INSERT INTO `users_permission_add` (`addID`, `groupID`, `menuID`, `subID`) VALUES
(4, 5, 2, 6),
(3, 5, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users_permission_delete`
--

CREATE TABLE `users_permission_delete` (
  `deleteID` int(11) NOT NULL,
  `groupID` int(11) DEFAULT NULL,
  `menuID` int(11) DEFAULT NULL,
  `subID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_permission_edit`
--

CREATE TABLE `users_permission_edit` (
  `editID` int(11) NOT NULL,
  `groupID` int(11) DEFAULT NULL,
  `menuID` int(11) DEFAULT NULL,
  `subID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users_permission_read`
--

CREATE TABLE `users_permission_read` (
  `readID` int(11) NOT NULL,
  `groupID` int(11) DEFAULT NULL,
  `menuID` int(11) DEFAULT NULL,
  `subID` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_permission_read`
--

INSERT INTO `users_permission_read` (`readID`, `groupID`, `menuID`, `subID`) VALUES
(2, 5, 1, 1),
(3, 6, 1, 1),
(4, 6, 1, 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branchID`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`countryID`);

--
-- Indexes for table `country_zone`
--
ALTER TABLE `country_zone`
  ADD PRIMARY KEY (`zoneID`);

--
-- Indexes for table `franchisee`
--
ALTER TABLE `franchisee`
  ADD PRIMARY KEY (`franchiseeID`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menuID`);

--
-- Indexes for table `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD PRIMARY KEY (`subID`);

--
-- Indexes for table `operations_manuals`
--
ALTER TABLE `operations_manuals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resources_images`
--
ALTER TABLE `resources_images`
  ADD PRIMARY KEY (`imagesID`);

--
-- Indexes for table `resources_images_category`
--
ALTER TABLE `resources_images_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`restaurantID`);

--
-- Indexes for table `training`
--
ALTER TABLE `training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_answer`
--
ALTER TABLE `training_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_choice`
--
ALTER TABLE `training_choice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_group`
--
ALTER TABLE `training_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usersID`);

--
-- Indexes for table `users_access`
--
ALTER TABLE `users_access`
  ADD PRIMARY KEY (`accessID`);

--
-- Indexes for table `users_group`
--
ALTER TABLE `users_group`
  ADD PRIMARY KEY (`groupID`);

--
-- Indexes for table `users_permission_add`
--
ALTER TABLE `users_permission_add`
  ADD PRIMARY KEY (`addID`);

--
-- Indexes for table `users_permission_delete`
--
ALTER TABLE `users_permission_delete`
  ADD PRIMARY KEY (`deleteID`);

--
-- Indexes for table `users_permission_edit`
--
ALTER TABLE `users_permission_edit`
  ADD PRIMARY KEY (`editID`);

--
-- Indexes for table `users_permission_read`
--
ALTER TABLE `users_permission_read`
  ADD PRIMARY KEY (`readID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branchID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `countryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `country_zone`
--
ALTER TABLE `country_zone`
  MODIFY `zoneID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `franchisee`
--
ALTER TABLE `franchisee`
  MODIFY `franchiseeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menuID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `menu_sub`
--
ALTER TABLE `menu_sub`
  MODIFY `subID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `operations_manuals`
--
ALTER TABLE `operations_manuals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `resources_images`
--
ALTER TABLE `resources_images`
  MODIFY `imagesID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `resources_images_category`
--
ALTER TABLE `resources_images_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `restaurantID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `training`
--
ALTER TABLE `training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `training_answer`
--
ALTER TABLE `training_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `training_choice`
--
ALTER TABLE `training_choice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `training_group`
--
ALTER TABLE `training_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `usersID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users_access`
--
ALTER TABLE `users_access`
  MODIFY `accessID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users_group`
--
ALTER TABLE `users_group`
  MODIFY `groupID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users_permission_add`
--
ALTER TABLE `users_permission_add`
  MODIFY `addID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_permission_delete`
--
ALTER TABLE `users_permission_delete`
  MODIFY `deleteID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_permission_edit`
--
ALTER TABLE `users_permission_edit`
  MODIFY `editID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_permission_read`
--
ALTER TABLE `users_permission_read`
  MODIFY `readID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
