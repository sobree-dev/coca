<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>News & Alerts</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="<?=site_url('news/promotions');?>">
                            <div class="sub active">New Promotions</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-1 detail-news">
            <div class="col-md-7">
                <img src="<?=base_url('image/news/img.jpg');?>" alt="" class="wfull">
            </div>
            <div class="col-md-5">
                <div class="detail">
                    <h3 class='hed'>Lorem ipsum dolor sit amet, consectetuer adipiscing</h3>
                    <div style="font-size: 13px;">
                        <p>12 Feb 2020 by xxxxxxxxxxx</p><br>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing
                        elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                        volutpat
                        Lorem ipsum dolor sit amet, consectetuer adipiscing
                        elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                        volutpat
                        Lorem ipsum dolor sit amet, consectetuer adipiscing
                        elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat
                        volutpat
                    </p>
                    <div class="text-center mt-4">
                        <a href="<?=site_url('news/promotions');?>" id="btn_start">
                            <button type="button" class="btn btn-success WidthFull">Close</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>