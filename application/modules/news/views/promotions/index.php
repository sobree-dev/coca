<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>News & Alerts</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="">
                            <div class="sub active">New Promotions</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row list-news">
            <?php
            for ($i=0; $i < 10; $i++) {
            ?>
            <div class="col-md-6 mb-4">
                <div class="item-image">
                    <div class="left">
                        <img src="<?=base_url('image/news/img.jpg');?>" alt="" class="img">
                    </div>
                    <div class="right">
                        <div class="hed">
                            <div class="dot-ellipsis" style="height: 50px;">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
                                sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                                vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                                Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
                                elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor
                                eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis,
                                feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.
                                Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.
                                Nam eget dui.
                            </div>
                        </div>

                        <div class="text">
                            <div class="dot-ellipsis" style="height: 55px;">
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis,
                                sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec,
                                vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.
                                Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus
                                elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor
                                eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis,
                                feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.
                                Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.
                                Nam eget dui.
                            </div>
                        </div>

                        <div class="read">
                            <div class="w">
                                <span>12 Feb 2020 by xxxxxxxxxxx</span>
                            </div>
                            <div class="w text-right">
                                <a href="<?=site_url('news/promotions/detail');?>">
                                    <button type="button" class="btn btn-success">Read more</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>

        <!-- ============================================================== -->
    </div>
</div>