<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('DBrecord');
	}

    private function seo()
	{
		$title          = "News & Alerts / Newsletter";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('news/newsletter/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => null,
            'link' => current_url()
        );
        $this->session->set_userdata('URL_Recipe',$sess_data);
    }

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'news',
            'header'  => 'header',
            'content' => 'news/newsletter/index',
            'footer'  => 'footer',
            'function'=>  array('news'),
        );
        // DBrecord //
        $DBrecord['table'] = 'users_group';
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $this->load->view('template/body', $data);
    }

    public function detail()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'news',
            'header'  => 'header',
            'content' => 'news/newsletter/detail',
            'footer'  => 'footer',
            'function'=>  array('news'),
        );
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'operations',
            'header'  => 'header',
            'content' => 'operations/recipe/form',
            'footer'  => 'footer',
            'function'=>  array('operations'),
        );
        // DBrecord //
        $DBrecord['id'] = array('groupID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = 'users_group';
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['title'] = $input['title'];

        if ($input['groupID']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'users_group';
        $this->DBrecord->insert($DBrecord);
        // DBrecord //
        redirect( $this->session->URL_Recipe['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $DBrecord['id'] = array('groupID'=>$input['groupID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'users_group';
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->URL_Recipe['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('groupID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = 'users_group';
        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->URL_Recipe['link'], 'refresh');
	}
    
}