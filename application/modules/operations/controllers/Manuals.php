<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manuals extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');
        
        $this->id = end($this->uri->segment_array());
        $this->table = 'operations_manuals';

	}

    private function seo()
	{
		$title          = "Operations / Manuals & Form";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('operations/manuals/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
        $sess_data['id'] = $this->id;
        $sess_data['link'] = current_url();
        if ($this->session->data_search['QUERY_STRING']) {
            $sess_data['link'] = current_url().'?'.$this->session->data_search['QUERY_STRING'];
        }
        $this->session->set_userdata('operations_manuals',$sess_data);
    }
    
    private function upload_image($file, $path) {
		$config['upload_path'] = $path;
		$config['allowed_types'] = '*';
		$config['max_size'] = '*';
		$config['max_width']  = '*';
		$config['max_height']  = '*';
		$config['encrypt_name']  = TRUE;
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($file)) {
			return array('file_name' => false); // ถ้าอัพโหลดไม่ได้ ไม่สามารถเรียกดูข้อมูลไฟล์ที่อัพได้
		}else{
            $file_name = $this->upload->data();  // ถ้าอัพโหลดได้ เราสามารถเรียกดูข้อมูลไฟล์ที่อัพได้
            return $file_name['file_name'];
		}
	}

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'operations',
            'header'  => 'header',
            'content' => 'manuals/index',
            'footer'  => 'footer',
            'function'=>  array('operations'),
        );
        $this->load->view('template/body', $data);
    }

    public function detail()
	{
        $this->thisURL();

        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'operations',
            'header'  => 'header',
            'content' => 'manuals/detail',
            'footer'  => 'footer',
            'function'=>  array('operations'),
        );

        // DBrecord //
        $DBrecord['id'] = array('branchID' => $this->id);
        $DBrecord['table'] = $this->table;
        $data['operations_manuals'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'operations',
            'header'  => 'header',
            'content' => 'manuals/form',
            'footer'  => 'footer',
            'function'=>  array('operations'),
        );
        // DBrecord //
        $DBrecord['id'] = array('manualsID' =>  $this->id);
        $DBrecord['order_by'] = array('manualsID' => 'DESC');
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['title'] = $input['title'];
        $value['detail'] = $input['detail'];
        $value['branchID'] = $input['branchID'];

        if ($input['manualsID']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['file_doc'] = $input['file_doc_hid'];
		if (isset($_FILES['file_doc']['name']) && !empty($_FILES['file_doc']['name'])) {
            $value['file_doc'] = $this->upload_image('file_doc', './uploads/operations/manuals/');
        }
        
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->operations_manuals['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $value['file_doc'] = $input['file_doc_hid'];
        if (isset($_FILES['file_doc']['name']) && !empty($_FILES['file_doc']['name'])) {
            $value['file_doc'] = $this->upload_image('file_doc', './uploads/operations/manuals/');
            if ($value['file_doc']) {
                unlink('./uploads/operations/manuals/'.$input['file_doc_hid']);
            }
        }

        $DBrecord['id'] = array('manualsID'=>$input['manualsID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->operations_manuals['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('manualsID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->file_doc) {
            unlink('./uploads/operations/manuals/'.$result->file_doc);
        }

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->operations_manuals['link'], 'refresh');
	}
    
}