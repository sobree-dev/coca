<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
$(document).ready(function() {
    $('.select-multiple').select2();
});
</script>
<style>
.select2-container--default .select2-selection--multiple .select2-selection__choice {
    color : #000;
}
</style>