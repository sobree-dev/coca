<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Detail</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Sort</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="sort" name="sort"
                                value="<?=isset($result->sort) ? $result->sort : '';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Title</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="title" name="title"
                                value="<?=isset($result->title)?$result->title:'';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">Detail</label>
                        <div class="col-md-10">
                            <textarea id="detail"
                                name="detail"><?=isset($result->detail)?$result->detail:'';?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">File</label>
                        <div class="col-md-10">
                        <input type="hidden" name="file_doc_hid" value="<?=!empty($result->pdf) ? $result->pdf : '';?>">
                            <div class="custom-file" id="customFile" lang="es">
                                <input type="file" class="custom-file-input" name="file_doc" accept="application/pdf">
                                <label class="custom-file-label" for="exampleInputFile">
                                <?=!empty($result->pdf) ? $result->pdf : 'Select file...';?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="offset-md-2 col-md-10">
                            <?php if ($this->session->recipe_detail['id']=='drink') { ?>
                            <input type="hidden" id="type" name="type"
                                value="drink">
                            <?php }else{ ?>
                            <input type="hidden" id="recipeID" name="recipeID"
                                value="<?=$this->session->recipe_detail['id'];?>">
                            <?php } ?>
                            <input type="hidden" id="detailID" name="detailID"
                                value="<?=isset($result->detailID) ? $result->detailID : '';?>">
                            <button type="submit" class="btn btn-info">SAVE</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>