<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Manuals & Form</h2>
                <hr>
            </div>
        </div>

        <div class="listdata">

            <?php if ($this->session->sess_login['type']=='Corporate') { ?>
            <div class="text-right">
                <a href="<?=$Urladd;?>">
                    <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                </a>
            </div>
            <?php } ?>
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    if(!empty($operations_manuals)){
                    foreach ($operations_manuals as $value) {
                    ?>
                    <tr>
                        <td>
                            <div class="tr-col">
                                <p class="head"><?=$value->title;?></p>
                                <?=$value->detail;?>
                            </div>
                        </td>
                        <td>
                            <div class="tr-col text-right">
                                <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                                <div class="mb-2">
                                    <a href="<?=$Urledit.'/'.$value->manualsID;?>">
                                        <button type="button" class="btn btn-warning">Edit</button>
                                    </a>
                                    <a href="javascript:void(0);" click-delete="<?=$Urldelete.'/'.$value->manualsID;?>">
                                        <button type="button" class="btn btn-danger">Delete</button>
                                    </a>
                                </div>
                                <?php } ?>
                                <a href="<?=base_url('uploads/operations/manuals/'.$value->file_doc);?>" target="_blank">
                                    <button type="button" class="btn btn-success">Download</button>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php  } }else{ ?>
                    </tr>
                    <div class="col-12 mt-3">
                        <div class="alert alert-warning text-center">
                            <strong>! </strong> Please select from drop down list.

                        </div>
                    </div>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>

        </div>

        <!-- ============================================================== -->
    </div>
</div>