<div id="ts_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" style="max-width: 80%;">
        <div class="modal-content" id="ts_modal_data">
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>
    $('a[data-toggle="modal"]').click(function (e) {
        var load = $(this).attr("data-load");
        $.ajax({
            url: load,
            method: "POST",
            data: {
                phoneData: 1
            },
            success: function (data) {
                $('#ts_modal_data').html(data);
                $('#ts_modal').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error..');
            }
        });
    });
</script>


<script>
$('select[change-country]').on('change', function(){
    $('body').loading();
    var sendURL = $(this).attr('change-country');
    var sendID = $(this).val();
    // alert(sendURL);
    if(sendID){
        $.ajax({
            type:'POST',
            url: sendURL,
            data:{ getID:sendID },
            success:function(html){
                $('#respon-zone').html(html);
                // alert(html);
                $('body').loading('stop');
            }
        }); 
    }
});
</script>