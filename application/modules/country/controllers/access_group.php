<?php
defined('BASEPATH') OR exit('No direct script access_group allowed');

class Access_group extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');

        $this->id = end($this->uri->segment_array());
        $this->table = 'users_group';
    }
    


    private function seo()
	{
		$title          = "Control system / Access_group";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('country/access_group/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => $this->id,
            'link' => current_url()
        );
        $this->session->set_userdata('access_group',$sess_data);
    }

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/access_group/index',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        // DBrecord //
        $DBrecord['id'] = array('accessID' => $this->id);
        $DBrecord['order_by'] = array('groupID' => 'DESC');
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/access_group/form',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        // DBrecord //
        $DBrecord['id'] = array('groupID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['title'] = $input['title'];
        $value['accessID'] = $input['accessID'];

        if ($input['groupID']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->access_group['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $DBrecord['id'] = array('groupID'=>$input['groupID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->access_group['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('groupID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->access_group['link'], 'refresh');
    }
    
    /////////// popup //
    public function popup()
	{
		$data = array(
			 'groupID'   => end($this->uri->segment_array()),
        );
		$this->load->view('country/access_group/popup', $data);
    }

    private function users_permission($usersID, $input, $table)
	{
        $this->db->where('groupID', $usersID);
        $this->db->delete($table);
        /////// delete
        foreach ($input as $_read) {
            $read = explode("|", $_read);
            $value['groupID'] = $usersID;
            $value['menuID'] = $read[0];
            $value['subID'] = $read[1];
            /////// insert
            $this->db->insert($table, $value);
        }
    }
    
    public function users_permission_save()
	{
        $input = $this->input->post();

        ///// read
        $this->users_permission($input['groupID'], $input['read'], 'users_permission_read');
        ///// add
        $this->users_permission($input['groupID'], $input['add'], 'users_permission_add');
        ///// edit
        $this->users_permission($input['groupID'], $input['edit'], 'users_permission_edit');
        ///// delete
        $this->users_permission($input['groupID'], $input['delete'], 'users_permission_delete');
        
        redirect( $this->session->access_group['link'], 'refresh');

        // var_dump($input);
    }
    
}