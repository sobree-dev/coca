<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class franchisee extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');
        
        $this->id = end($this->uri->segment_array());
        $this->table = 'franchisee';
	}

    private function seo()
	{
		$title          = "franchisee / Franchisee";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('country/franchisee/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => null,
            'link' => current_url()
        );
        $this->session->set_userdata('URL_Franc',$sess_data);
        $this->session->set_userdata('back_users',$sess_data);
	}

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/franchisee/index',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        // DBrecord //
        if ($this->session->sess_login['type']=='Franchisee') {
            $DBrecord['id'] = array('franchiseeID' => $this->session->sess_login['typeID']);
        }
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $data['Urlaction'] = site_url('country/users/index/Franchisee');
        $data['url_restaurant'] = site_url('country/restaurant/index');
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'country',
            'header'  => 'header',
            'content' => 'country/franchisee/form',
            'footer'  => 'footer',
            'function'=>  array('country'),
        );
        // DBrecord //
        $DBrecord['id'] = array('franchiseeID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['title'] = $input['title'];

        if ($input['franchiseeID']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // DBrecord //
        redirect( $this->session->URL_Franc['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $DBrecord['id'] = array('franchiseeID'=>$input['franchiseeID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->URL_Franc['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('franchiseeID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->URL_Franc['link'], 'refresh');
	}
    
}