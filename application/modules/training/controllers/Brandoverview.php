<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brandoverview extends MX_Controller {

    public function __construct()
	{
        parent::__construct();
		// $this->load->model('Orientation_m');
        $this->load->model('DBrecord');
        $this->table = 'training_group';
	}

    private function seo()
	{
		$title          = "Brandoverview";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }
    
    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('training/brandoverview/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
        $sess_data['id'] = $this->uri->segment(4);
        $sess_data['link'] = current_url();
        $sess_data['title'] = 'Brand Overview';
        if ($this->uri->segment(4)=='foodbeverage') {
            $sess_data['title'] = 'Back of House';
        }
        if ($this->uri->segment(4)=='service') {
            $sess_data['title'] = 'Front of House';
        }
        $this->session->set_userdata('training',$sess_data);
    }

    private function upload_image($file, $path) {
		$config['upload_path'] = $path;
		$config['allowed_types'] = '*';
		$config['max_size'] = '*';
		$config['max_width']  = '*';
		$config['max_height']  = '*';
		$config['encrypt_name']  = TRUE;
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($file)) {
			return array('file_name' => false); // ถ้าอัพโหลดไม่ได้ ไม่สามารถเรียกดูข้อมูลไฟล์ที่อัพได้
		}else{
            $file_name = $this->upload->data();  // ถ้าอัพโหลดได้ เราสามารถเรียกดูข้อมูลไฟล์ที่อัพได้
            $this->load->library('image_lib');
            $configer =  array(
                'image_library'   => 'gd2',
                'source_image'    =>  $file_name['full_path'],
                'maintain_ratio'  =>  TRUE,
                'width'           =>  900,
                'height'          =>  900,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();
            return $file_name['file_name'];
		}
	}

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'brandoverview/training',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        // DBrecord //
        $DBrecord['order_by'] = array('sort' => 'asc');
        $DBrecord['id'] = array('groups' => 'brandoverview');
        if ($this->uri->segment(4)) {
            $DBrecord['id'] = array('groups' => $this->uri->segment(4));
        }
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $data['Urlaction'] = site_url('training/training_question/index');
        $data['Urlaction2'] = site_url('training/training_images/index');
        $this->load->view('template/body', $data);
    }

    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'brandoverview/form',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        // DBrecord //
        $DBrecord['id'] = array('id' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }

    public function _build_data($input)
	{
        $value['sort'] = $input['sort'];
        $value['title'] = $input['title'];
        $value['groups'] = 'brandoverview';
        if ($this->session->training['id']) {
            $value['groups'] = $this->session->training['id'];
        }
        $value['detail'] = $input['detail'];
        $value['total'] = $input['total'];
        $value['must'] = $input['must'];

        if ($input['id']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

    public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['pdf'] = $input['file_doc_hid'];
        if (isset($_FILES['file_doc']['name']) && !empty($_FILES['file_doc']['name'])) {
            $value['pdf'] = $this->upload_image('file_doc', './uploads/training/brandoverview/');
        }
        
        $value['image'] = $input['file_img_hid'];
        if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/training/brandoverview/');
        }

        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->training['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['pdf'] = $input['file_doc_hid'];
        if (isset($_FILES['file_doc']['name']) && !empty($_FILES['file_doc']['name'])) {
            $value['pdf'] = $this->upload_image('file_doc', './uploads/training/brandoverview/');
            if ($value['pdf']) {
                unlink('./uploads/training/brandoverview/'.$input['file_doc_hid']);
            }
        }

        $value['image'] = $input['file_img_hid'];
        if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/training/brandoverview/');
            if ($value['image']) {
                unlink('./uploads/training/brandoverview/'.$input['file_img_hid']);
            }
        }

        $DBrecord['id'] = array('id'=>$input['id']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->training['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('id' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->pdf) {
            unlink('./uploads/training/brandoverview/'.$result->pdf);
        }
        if ($result->image) {
            unlink('./uploads/training/brandoverview/'.$result->image);
        }

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->training['link'], 'refresh');
	}


	public function detail()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'brandoverview/detail',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        $this->load->view('template/body', $data);
	}

	public function choice()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'brandoverview/choice',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        $this->load->view('template/body', $data);
	}

	public function send()
	{
        // var_dump( $this->input->post() );
        $input = $this->input->post();
        foreach ($input as $key => $values) {
            // แยกค่าและรวมดาค้า
            $value = explode('|', $values);
            $data = array(
                'user_id' => $this->session->sess_login['usersID'],
                'training_id' => $value[0],
                'training_choice_id' => $value[1],
                'score' => $value[2],
            );
            // หาค่า ว่าเคยทดสอบข้อนี้หรือไม่
            $this->db->where('training_id', $value[0]);
            $training_answer = $this->db->get('training_answer');
            if ($training_answer->num_rows()>0) {
                $this->db->where('training_id', $value[0]);
                $this->db->update('training_answer', $data);
            } else {
                $this->db->insert('training_answer', $data);
            } 
        }
        redirect( $this->session->training['link'], 'refresh');
    }
    
}