<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orientation extends MX_Controller {

	public function __construct()
	{
        parent::__construct();
		// $this->load->model('Orientation_m');
        $this->load->model('DBrecord');
        $this->table = 'training_group';
	}

    private function seo()
	{
		$title          = "Orientation";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }
    
    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('training/orientation/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
        $sess_data['id'] = null;
        $sess_data['link'] = current_url();
        $sess_data['title'] = 'Orientation';
        $this->session->set_userdata('training',$sess_data);
    }

    private function upload_image($file, $path) {
		$config['upload_path'] = $path;
		$config['allowed_types'] = '*';
		$config['max_size'] = '*';
		$config['max_width']  = '*';
		$config['max_height']  = '*';
		$config['encrypt_name']  = TRUE;
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($file)) {
			return array('file_name' => false); // ถ้าอัพโหลดไม่ได้ ไม่สามารถเรียกดูข้อมูลไฟล์ที่อัพได้
		}else{
            $file_name = $this->upload->data();  // ถ้าอัพโหลดได้ เราสามารถเรียกดูข้อมูลไฟล์ที่อัพได้
            $this->load->library('image_lib');
            $configer =  array(
                'image_library'   => 'gd2',
                'source_image'    =>  $file_name['full_path'],
                'maintain_ratio'  =>  TRUE,
                'width'           =>  900,
                'height'          =>  900,
            );
            $this->image_lib->clear();
            $this->image_lib->initialize($configer);
            $this->image_lib->resize();
            return $file_name['file_name'];
		}
	}

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'orientation/training',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        // DBrecord //
        $DBrecord['order_by'] = array('sort' => 'asc');
        $DBrecord['id'] = array('groups' => 'orientation');
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $data['Urlaction'] = site_url('training/training_question/index');
        $this->load->view('template/body', $data);
    }

    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'orientation/form',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        // DBrecord //
        $DBrecord['id'] = array('id' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }

    public function _build_data($input)
	{
        $value['sort'] = $input['sort'];
        $value['title'] = $input['title'];
        $value['groups'] = 'orientation';
        $value['detail'] = $input['detail'];
        $value['total'] = $input['total'];
        $value['must'] = $input['must'];

        if ($input['id']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

    public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['video'] = $input['file_doc_hid'];
		if (isset($_FILES['file_doc']['name']) && !empty($_FILES['file_doc']['name'])) {
            $value['video'] = $this->upload_image('file_doc', './uploads/training/orientation/');
        }

        $value['image'] = $input['file_img_hid'];
		if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/training/orientation/');
        }
        
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->training['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $value['video'] = $input['file_doc_hid'];
        if (isset($_FILES['file_doc']['name']) && !empty($_FILES['file_doc']['name'])) {
            $value['video'] = $this->upload_image('file_doc', './uploads/training/orientation/');
            if ($value['video']) {
                unlink('./uploads/training/orientation/'.$input['file_doc_hid']);
            }
        }

        $value['image'] = $input['file_img_hid'];
        if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/training/orientation/');
            if ($value['image']) {
                unlink('./uploads/training/orientation/'.$input['file_img_hid']);
            }
        }

        $DBrecord['id'] = array('id'=>$input['id']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->training['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('id' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->video) {
            unlink('./uploads/training/orientation/'.$result->video);
        }
        if ($result->image) {
            unlink('./uploads/training/orientation/'.$result->image);
        }

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->training['link'], 'refresh');
	}
    
    // public function loadRecord($rowno=0){

	// 	// Row per page
	// 	$rowperpage = 3;
	// 	// Row position
	// 	if($rowno != 0){
	// 		$rowno = ($rowno-1) * $rowperpage;
	// 	}
      	
    //   	// All records count
    //   	$allcount = $this->Orientation_m->pagination_getCount();
    //   	// Get  records
    //     $users_record = $this->Orientation_m->pagination_getData($rowno,$rowperpage);
          
    //     // with bootstrap 4
    //     $this->config->load('pagination_custom', TRUE);
    //     $config = $this->config->item('pagination_custom');
      	
    //   	// Pagination Configuration
    //   	$config['base_url'] = $this->input->get('Url');
    //   	$config['use_page_numbers'] = TRUE;
	// 	$config['total_rows'] = $allcount;
	// 	$config['per_page'] = $rowperpage;
	// 	// Initialize
	// 	$this->pagination->initialize($config);
	// 	// Initialize $data Array
	// 	$data['pagination'] = $this->pagination->create_links();
	// 	$data['result'] = $users_record;
    //     $data['row'] = $rowno;

	// 	$data['loadRecordAjax'] = $this->loadRecordAjax($users_record);

	// 	echo json_encode($data);
		
    // }

    // public function loadRecordAjax($users_record){
    //     // $result_view = print_r($result);
    //     $data['result'] = $users_record;
    //     return $this->load->view('orientation/training_ajax', $data, true);
    // }


	public function detail()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'orientation/detail',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        $this->load->view('template/body', $data);
	}

	public function choice()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'training',
            'header'  => 'header',
            'content' => 'orientation/choice',
            'footer'  => 'footer',
            'function'=>  array('training'),
        );
        $this->load->view('template/body', $data);
	}

	public function send()
	{
        // var_dump( $this->input->post() );
        $input = $this->input->post();
        foreach ($input as $key => $values) {
            // แยกค่าและรวมดาค้า
            $value = explode('|', $values);
            $data = array(
                'user_id' => $this->session->sess_login['usersID'],
                'training_id' => $value[0],
                'training_choice_id' => $value[1],
                'score' => $value[2],
            );
            // หาค่า ว่าเคยทดสอบข้อนี้หรือไม่
            $this->db->where('training_id', $value[0]);
            $training_answer = $this->db->get('training_answer');
            if ($training_answer->num_rows()>0) {
                $this->db->where('training_id', $value[0]);
                $this->db->update('training_answer', $data);
            } else {
                $this->db->insert('training_answer', $data);
            } 
        }
        redirect(site_url('training/orientation'), 'refresh');
    }
    
}