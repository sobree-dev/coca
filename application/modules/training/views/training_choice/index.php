<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Choice</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="<?=$this->session->training['link'];?>">
                            <div class="sub"><?=$this->session->training['title'];?></div>
                        </a>
                        <a href="<?=$this->session->training_question['link'];?>">
                            <div class="sub">Question</div>
                        </a>
                        <a href="">
                            <div class="sub active">Choice</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="text-right">
                    <a href="<?=$Urladd;?>">
                        <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                    </a>
                </div>

                <div class="listdata">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>title</th>
                                <th>answer</th>
                                <th class="text-right">Action</th>
                                <!-- <th class="text-center"></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($result as $val) {
                                $answer = 'NO';
                                if ($val->answer==1) {
                                    $answer = 'YES';
                                }
                            ?>
                            <tr>
                                <td>
                                    <div class="tr-col"><?=$val->sort;?></div>
                                </td>
                                <td>
                                    <div class="tr-col"><?=$val->detail;?></div>
                                </td>
                                <td>
                                    <div class="tr-col"><?=$answer;?></div>
                                </td>
                                <td>
                                    <div class="tr-col text-right">
                                        <a href="<?=$Urledit.'/'.$val->id;?>">
                                            <button type="button" class="btn btn-warning">Edit</button>
                                        </a>
                                        <a href="javascript:void(0);" click-delete="<?=$Urldelete.'/'.$val->id;?>">
                                            <button type="button" class="btn btn-danger">Delete</button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>