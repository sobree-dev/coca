<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Choice</h2>
                <hr><br>
                <?php echo form_open_multipart($Urlform); ?>
                <div class="form-group">
                    <div class="row">
                        <label for="result" class="col-md-2 control-label">sort</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" id="sort" name="sort"
                                value="<?=isset($result->sort) ? $result->sort : '';?>" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">detail</label>
                        <div class="col-md-10">
                            <textarea id="detail" name="detail"
                                required><?=isset($result->detail)?$result->detail:'';?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="title" class="col-md-2 control-label">answer</label>
                        <div class="col-md-10">
                            <!-- <textarea id="title" name="title" required><?=isset($result->title)?$result->title:'';?></textarea> -->
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="answer" value="0"
                                        <?php if ($result->answer==0) {echo 'checked';}?>>NO
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="answer" value="1"
                                        <?php if ($result->answer==1) {echo 'checked';}?>>YES
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="offset-md-2 col-md-10">
                            <input type="hidden" id="training_id" name="training_id"
                                value="<?=$this->session->training_choice['id']?>">
                            <input type="hidden" id="id" name="id" value="<?=isset($result->id) ? $result->id : '';?>">
                            <button type="submit" class="btn btn-info">SAVE</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>