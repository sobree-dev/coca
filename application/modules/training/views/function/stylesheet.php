<!-- sweetalert2 -->
<link rel="stylesheet" href="<?=base_url('plugin/sweetalert2/sweetalert2.min.css'); ?>">
<!-- video -->
<link rel="stylesheet" href="<?=base_url('plugin/video/plyr.css'); ?>">
<!-- fancybox3 -->
<link rel="stylesheet" href="<?=base_url('plugin/fancybox3/dist/jquery.fancybox.css'); ?>">

<!-- owlcarousel -->
<link rel="stylesheet" href="<?=base_url('plugin/owlcarousel/owlcarousel/assets/owl.theme.default.min.css'); ?>">
<link rel="stylesheet" href="<?=base_url('plugin/owlcarousel/owlcarousel/assets/owl.carousel.css'); ?>">