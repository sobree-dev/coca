<!-- sweetalert2 -->
<script type="text/javascript" src="<?=base_url('plugin/sweetalert2/sweetalert2.min.js');?>"></script>
<!-- validate -->
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>
<!-- fancybox3 -->
<script type="text/javascript" src="<?=base_url('plugin/fancybox3/dist/jquery.fancybox.min.js');?>"></script>

<script>
    function Swal_warning() {
        Swal.fire({
            title: 'Please select',
            text: 'Please select before pressing Next.',
            type: 'warning',
        });
    }
</script>

<script>
    $('a[id="btn_submit"]').click(function (e) {
        var datarow = $(this).attr('data-rows');
        if (!$('[name="choice' + datarow + '"]').is(':checked')) {
            Swal_warning();
        } else {
            $('#training_form').submit();
        }
    });
</script>

<script>
    $('a[id="btn_back"]').click(function (e) {
        var datarow = $(this).attr('data-rows');
        var datarow_now = parseInt(datarow) - parseInt(1);

        $('div[col-train]').css("display", "none");
        $('div[col-train="training_' + datarow_now + '"]').css("display", "block");
    });
</script>

<script>
    $('a[id="btn_next"]').click(function (e) {
        var datarow = $(this).attr('data-rows');
        var datarow_now = parseInt(datarow) + parseInt(1);

        if (!$('[name="choice' + datarow + '"]').is(':checked')) {
            Swal_warning();
        } else {
            $('div[col-train]').css("display", "none");
            $('div[col-train="training_' + datarow_now + '"]').css("display", "block");
        }
    });
</script>

<!-- video -->
<script type="text/javascript" src="<?=base_url('plugin/video/video.js');?>"></script>
<script type="text/javascript" src="<?=base_url('plugin/video/plyr.js');?>"></script>
<script type="text/javascript" src="<?=base_url('plugin/video/jquery.visible.min.js');?>"></script>

<script>
    $('video#training').on('timeupdate', function (e) {
        var currentPos = $(this)[0].currentTime; //Get currenttime
        var maxduration = $(this)[0].duration; //Get video duration
        var percentage = 100 * currentPos / maxduration; //in %
        console.log(currentPos);
        console.log(percentage);
        if (percentage > 98) {
            // alert(111);
            // Swal.fire({
            //     title: 'You have watched the video.',
            //     text: 'Can do the test immediately',
            //     type: 'success',
            // });
            $("#btn_start button").prop('disabled', false);
            $("#btn_start button").removeClass('btn-secondary');
            $("#btn_start button").addClass('btn-info');
            e.stopPropagation();
        }
    });
</script>

<script language="javascript">
    var count=0;
    var timerVar=null;
    window.onload=function(){
        timerVar=setTimeout("setTimer()",1000);
    }

    function setTimer(){
        clearTimeout(timerVar);
        count+=1;
        timerVar=setTimeout("setTimer()",1000);

        $('span#timetotal').html(secondsTimeSpanToHMS(count));
        $('input#timetotal').val(secondsTimeSpanToHMS(count));
    }
    function secondsTimeSpanToHMS(s) {
        var h = Math.floor(s/3600); //Get whole hours
        s -= h*3600;
        var m = Math.floor(s/60); //Get remaining minutes
        s -= m*60;
        return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
    }
    // console.log(count);
</script>

<!-- owlcarousel -->
<script type="text/javascript" src="<?=base_url('plugin/owlcarousel/owlcarousel/owl.carousel.js');?>"></script>
<script type="text/javascript" src="<?=base_url('plugin/owlcarousel/vendors/highlight.js');?>"></script>
<script type="text/javascript" src="<?=base_url('plugin/owlcarousel/js/app.js');?>"></script>
<script>
$('.owl-carousel').owlCarousel({
    loop:false,
    margin:10,
    nav:true,
    dots: false,
    items:1,
    autoHeight:true,
    navText: ["<img src='<?=base_url('image/logo/back.png');?>'>",
			"<img src='<?=base_url('image/logo/next.png');?>'>"
		]
})
</script>