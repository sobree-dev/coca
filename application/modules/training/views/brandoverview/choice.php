<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">
        <h2><?=$this->session->training['title'];?></h2>
        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="text-right">
                    <div>
                        <!-- <b class="h5">Timer = </b><span id="timetotal">0:00:00</span> -->
                        <input type="hidden" name="timetotal" id="timetotal"/>
                    </div>
                </div>
                <form id="training_form" action="<?=site_url('training/brandoverview/send');?>" method="post">
                    <?php
                    $rows = 1;
                    $this->db->where('training_group_id', base64_decode($this->uri->segment(4)));
                    $training = $this->db->get('training');
                    $num_rows = $training->num_rows();
                    foreach ($training->result() as $val_training) {
                    ?>
                    <div class="training" col-train="training_<?=$rows;?>"
                        style="display: <?php if($rows!=1){echo 'none';}?>;">
                        <div class="clauses">
                            <span><?=$rows;?> / <?=$num_rows;?></span>
                        </div>
                        <div class="head">
                            <span><?=$val_training->title;?></span>
                        </div>

                        <div class="row">
                            <?php
                            $this->db->where('training_id', $val_training->id);
                            $training_choice = $this->db->get('training_choice');
                            foreach ($training_choice->result() as $val_choice) {
                            ?>
                            <div class="col-lg-6 col-md-12">
                                <label class="radio">
                                    <input type="radio" name="choice<?=$rows;?>"
                                        value="<?=$val_choice->training_id.'|'.$val_choice->id.'|'.$val_choice->answer;?>">
                                    <span>
                                        <?=$val_choice->detail;?>
                                    </span>
                                </label>
                            </div>
                            <?php } ?>

                            <div class="col-md-12 text-right">
                                <a data-fancybox data-src="#VDOifram" href="javascript:;">
                                    <button type="button" class="btn btn-primary">View slide</button>
                                </a>
                                <?php if ($rows==$num_rows) { ?>
                                <a href="javascript:;" id="btn_back" data-rows="<?=$rows;?>">
                                    <button type="button" class="btn btn-secondary"><i
                                            class="fas fa-angle-double-left"></i> BACK</button>
                                </a>
                                <a href="javascript:;" id="btn_submit" data-rows="<?=$rows;?>">
                                    <button type="button" class="btn btn-info">Complete <i
                                            class="far fa-paper-plane"></i></button>
                                </a>
                                <?php } else if($rows==1) { ?>
                                <a href="javascript:;" id="btn_next" data-rows="<?=$rows;?>">
                                    <button type="button" class="btn btn-success">NEXT <i
                                            class="fas fa-angle-double-right"></i></button>
                                </a>
                                <?php } else { ?>
                                <a href="javascript:;" id="btn_back" data-rows="<?=$rows;?>">
                                    <button type="button" class="btn btn-secondary"><i
                                            class="fas fa-angle-double-left"></i> BACK</button>
                                </a>
                                <a href="javascript:;" id="btn_next" data-rows="<?=$rows;?>">
                                    <button type="button" class="btn btn-success">NEXT <i
                                            class="fas fa-angle-double-right"></i></button>
                                </a>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                    <?php $rows++; } ?>
                </form>
            </div>
        </div>
        
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>

<div style="display: none;width: 70%;" id="VDOifram">
    <div>
    <div class="owl-carousel owl-theme mt-1">
        <?php
        $this->db->where('groupID', base64_decode($this->uri->segment(4)));
        $this->db->from('training_images');
        $resources_images = $this->db->get()->result();
        if(!empty($resources_images)){
        $numItems = count($resources_images);
        $i = 0;
        foreach ($resources_images as $key=>$value) {
        ?>
        <div class="item">
            <img src="<?=base_url('uploads/training/brandoverview/'.$value->files);?>" class="wfull" alt="" style="max-height: 80vh;">
        </div>
        <?php } ?>
        <?php } ?>
    </div>
    </div>
</div>

