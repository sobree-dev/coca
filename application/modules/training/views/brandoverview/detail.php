<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2><?=$this->session->training['title'];?></h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="<?=site_url('training/orientation');?>">
                            <div class="sub active">Brand Overview</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $this->db->where('id', base64_decode($this->uri->segment(4)));
        $training_group = $this->db->get('training_group');
        foreach ($training_group->result() as $val) {
        ?>
        <div class="row mt-1">
            <div class="col-md-12">
                <h3><?=$val->title;?></h3>
                <?=$val->detail;?>
                <hr>
                <span style="color: #f00;">* Please see all images to take the test.</span>
                <?php if($this->uri->segment(5)) {?>
                <!-- <div class="text-center mt-4">
                    <a href="<?=site_url('training/brandoverview/choice/'.base64_encode($val->id));?>"
                        id="btn_start">
                        <button type="button" class="btn btn-info WidthFull"
                            style="font-size: 25px;">Question <i class="fas fa-tasks"></i></button>
                    </a>
                </div> -->
                <?php } ?>
                <div class="owl-carousel owl-theme mt-1">
                    <?php
                    $this->db->where('groupID', base64_decode($this->uri->segment(4)));
                    $this->db->from('training_images');
                    $resources_images = $this->db->get()->result();
                    if(!empty($resources_images)){
                    $numItems = count($resources_images);
                    $i = 0;
                    foreach ($resources_images as $key=>$value) {
                    ?>
                    <div class="item" style="padding: 0 100px;">
                        <div class="text-center my-1">
                            <?php if(++$i === $numItems) {?>
                            <a href="<?=site_url('training/brandoverview/choice/'.base64_encode($val->id));?>"
                                id="btn_start">
                                <button type="button" class="btn btn-info WidthFull" style="font-size: 25px;">Question
                                    <i class="fas fa-tasks"></i></button>
                            </a>
                            <?php }else{ ?>
                            <a href="<?=site_url('training/brandoverview/choice/'.base64_encode($val->id));?>"
                                id="btn_start">
                                <button type="button"
                                    class="btn <?php if(!$this->uri->segment(5)){echo 'btn-secondary';}else{echo 'btn-info';}?> WidthFull"
                                    style="font-size: 25px;"
                                    <?php if(!$this->uri->segment(5)){echo 'disabled';}?>>Question <i
                                        class="fas fa-tasks"></i></button>
                            </a>
                            <?php } ?>
                        </div>
                        <img src="<?=base_url('uploads/training/brandoverview/'.$value->files);?>" class="wfull" alt="">
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>

        </div>
        <?php } ?>

        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>