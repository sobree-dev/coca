<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2><?=$this->session->training['title'];?></h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <!-- <a href="<?=site_url('training/orientation');?>">
                            <div class="sub">Orientation</div>
                        </a> -->
                        <a href="">
                            <div class="sub active"><?=$this->session->training['title'];?></div>
                        <!-- </a>
                        <div class="sub">Management Training</div>
                        <div class="sub">Food & Beverage</div>
                        <div class="sub">Services</div> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                <div class="text-right">
                    <a href="<?=$Urladd;?>">
                        <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                    </a>
                </div>
                <?php } ?>

                <div class="listdata">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Training</th>
                                <th class="min-sm">Description </th>
                                <th class="min-sm">Score</th>
                                <!-- <th class="min-sm">TIME (MIN)</th> -->
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            function training_answer($group_id=null) {
                            // เชื่อมต่อ training
                            $CI =& get_instance();
                            $CI->db->from('training a'); 
                            $CI->db->where('a.training_group_id', $group_id);
                            $training = $CI->db->get(); 
                            foreach ($training->result() as $val_training) {
                                // เชื่อมต่อ training_answer
                                $CI->db->from('training_answer a'); 
                                $CI->db->where('a.user_id', $CI->session->sess_login['usersID']);
                                $CI->db->where('a.training_id', $val_training->id);
                                $training_choice = $CI->db->get(); 
                                $val_training_choice = $training_choice->row(); 
                                // $score = array();
                                if ($training_choice->num_rows()>0) {
                                    $score[] = $val_training_choice->score;
                                }
                            }
                            // รวมคะแนน
                            if ($training->num_rows()>0&&$training_choice->num_rows()>0) {
                                $array_sum = array_sum($score);
                                return (int)$array_sum;
                            }else{
                                return null;
                            }
                        }

                        foreach ($result as $key => $val) {
                            $training_answer = training_answer($val->id);
                            if (!isset($training_answer)) {
                                $status = $val->total.' Question|info|Begin';
                            } else if($training_answer>=$val->must) {
                                $status = 'Passed|success|Redo';
                            } else {
                                $status = 'Failed|danger|Redo';
                            }
                            $status_a = explode('|', $status);
                            // echo $status;
                        ?>
                            <tr>
                                <td>
                                    <div class="tr-col"><?=$val->sort;?></div>
                                </td>
                                <td>
                                    <div class="tr-col">
                                        <img src="<?=base_url('uploads/training/brandoverview/'.$val->image);?>" style="width: 100px;" alt="">
                                    </div>
                                </td>
                                <td>
                                    <div class="tr-col">
                                        <div class="text_dotdot">
                                            <div class="excerpt dot-ellipsis">
                                                <!-- <span class="me <?php if ($key==0) {echo 'active';} ?>"><i class="fas fa-circle"></i></span> -->
                                                <?=$val->title;?>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="tr-col min-sm">
                                        <div class="text_dotdot" style="max-width: 140px;">
                                            <div class="excerpt dot-ellipsis">
                                                <?=$val->detail;?>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="tr-col min-sm">
                                        <h4>
                                            <span class="badge badge-<?=$status_a[1];?>">
                                                <?=$status_a[0];?>
                                                <?php if(training_answer($val->id)){echo training_answer($val->id).' / '.$val->total;};?>
                                            </span>
                                            <?php if ($status_a[0]=='Passed'||$status_a[0]=='Failed') { ?>
                                                <a href="<?=base_url('uploads/training/brandoverview/'.$val->pdf);?>" download>
                                                    <div style="font-size: 14px;margin-top: 8px;"><i class="fas fa-download"></i> download</div>
                                                </a>
                                            <?php } ?>
                                        </h4>
                                    </div>
                                </td>
                                <!-- <td>
                                    <div class="tr-col min-sm">12.00</div>
                                </td> -->
                                <td>
                                    <div class="tr-col text-center">
                                        <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                                        <div class="mb-2">
                                            <div class="dropdown">
                                                <button class="btn btn-warning dropdown-toggle" type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                    <i class="fas fa-list"></i> More...
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="<?=$Urlaction.'/'.$val->id;?>"><i class="fas fa-plus"></i> Add Question</a>
                                                    <a class="dropdown-item" href="<?=$Urlaction2.'/'.$val->id;?>"><i class="fas fa-plus"></i> Add Image</a>
                                                    <a class="dropdown-item" href="<?=$Urledit.'/'.$val->id;?>"><i class="far fa-edit"></i> Edit</a>
                                                    <a class="dropdown-item"  href="javascript:void(0);" click-delete="<?=$Urldelete.'/'.$val->id;?>"><i class="far fa-trash-alt"></i> Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <?php
                                        $link_q = null;
                                        if(training_answer($val->id)){ $link_q = 'ready'; }
                                        ?>
                                        <a
                                            href="<?=site_url('training/brandoverview/detail/'.base64_encode($val->id).'/'.$link_q);?>">
                                            <button type="button"
                                                class="btn btn-<?=$status_a[1];?>"><?=$status_a[2];?></button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

                <div class="mt-4" id="pagination"></div>

            </div>
        </div>

        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>