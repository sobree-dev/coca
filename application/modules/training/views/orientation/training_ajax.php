    <?php
    function training_answer($group_id=null) {
        // เชื่อมต่อ training
        $CI =& get_instance();
        $CI->db->from('training a'); 
        $CI->db->where('a.training_group_id', $group_id);
        $training = $CI->db->get(); 
        foreach ($training->result() as $val_training) {
            // เชื่อมต่อ training_answer
            $CI->db->from('training_answer a'); 
            $CI->db->where('a.training_id', $val_training->id);
            $training_choice = $CI->db->get(); 
            $val_training_choice = $training_choice->row(); 
            if ($training_choice->num_rows()>0) {
                $score[] = $val_training_choice->score;
            }
        }
        // รวมคะแนน
        if ($training->num_rows()>0&&$training_choice->num_rows()>0) {
            $array_sum = array_sum($score);
            return (int)$array_sum;
        }else{
            return null;
        }
    }

    foreach ($result as $key => $val) {
        $training_answer = training_answer($val['id']);
        if (!isset($training_answer)) {
            $status = ''.$val['total'].' Question|info|Begin';
        } else if($training_answer>=$val['must']) {
            $status = 'Completed|success|Redo';
        } else {
            $status = 'Incompleted|danger|Redo';
        }
        $status_a = explode('|', $status);
        // echo $status;
    ?>
    <tr>
        <td>
            <div class="tr-col">
                <div class="text_dotdot">
                    <div class="excerpt dot-ellipsis">
                        <span class="me <?php if ($key==0) {echo 'active';} ?>"><i class="fas fa-circle"></i></span>
                        <?=$val['title'];?>
                    </div>
                </div>
            </div>
        </td>
        <td>
            <div class="tr-col min-sm"><?=$val['detail'];?></div>
        </td>
        <!-- <div class="blog min-sm"><?=$val['total'];?></div>
        <div class="blog min-sm"><?=$val['must'];?></div> -->
        <td>
            <div class="tr-col">
                <h4>
                    <span class="badge badge-<?=$status_a[1];?>">
                        <?=$status_a[0];?>
                        <?php if(training_answer($val['id'])){echo training_answer($val['id']).' / '.$val['total'];};?>
                    </span>
                </h4>
            </div>
        </td>
        <td>
            <div class="tr-col min-sm">12.00</div>
        </td>
        <td>
            <div class="tr-col text-center">
                <a href="<?=site_url('training/orientation/detail/'.base64_encode($val['id']));?>">
                    <button type="button" class="btn btn-<?=$status_a[1];?>"><?=$status_a[2];?></button>
                </a>
            </div>
        </td>
    </tr>
    <?php } ?>

    <!-- dottext -->
    <script src="<?=base_url('plugin/dottext/dottext.js');?>"></script>