<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="page-wrapper operations_manuals">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Brand Overview</h2>
                <hr>
            </div>
        </div>

        <!-- <form action="<?=current_url();?>" method="post" enctype="multipart/form-data"> -->
        <form action="<?=$url_form;?>" method="post" enctype="multipart/form-data">
            <div style="background: #556080;padding: 15px;">
                <div class="row">
                    <div class="col-8">
                        <div class="custom-file" id="customFile" lang="es">
                            <input type="file" class="custom-file-input" name="filename[]" accept="image/*" multiple>
                            <label class="custom-file-label" for="exampleInputFile">
                                Select file...
                            </label>
                        </div>
                    </div>
                    <div class="col-4">
                        <input type="hidden" id="groupID" name="groupID"
                            value="<?=$this->session->training_images['id'];?>">
                        <button type="submit" class="btn btn-success"><i class="fas fa-cloud-upload-alt"></i> UPLOAD</button>
                    </div>
                </div>
            </div>
        </form>

        <div class="row listdata">
            <?php
            $this->db->where('groupID', $this->session->training_images['id']);
            $this->db->from('training_images');
            $resources_images = $this->db->get()->result();
            if(!empty($resources_images)){
            foreach ($resources_images as $value) {
            ?>
            <div class="col-lg-2 col-md-3 col-sm-4 my-4">
                <a href="<?=base_url('uploads/training/brandoverview/'.$value->files);?>" data-fancybox="images"
                    data-caption="resources">
                    <div class="item-image boder">
                        <img src="<?=base_url('uploads/training/brandoverview/'.$value->files);?>" alt="" class="img"
                            style="padding: 0;">
                    </div>
                </a>
                <div class="text-center">
                    <a href="javascript:void(0);"
                        click-delete="<?=$url_delete.'/'.$value->imagesID;?>">
                        <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                    </a>
                    <a href="<?=base_url('uploads/resources/images/'.$value->files);?>" download>
                        <button type="button" class="btn btn-success"><i class="fas fa-download"></i></button>
                    </a>
                </div>
            </div>
            <?php  } }else{ ?>
            <div class="col-12 mt-3">
                <div class="alert alert-warning text-center">
                    <strong>! </strong> Please select from drop down list.

                </div>
            </div>
            <?php  } ?>
            <!-- Paginate -->
            <!-- <div class="mt-4" id="pagination"></div> -->
        </div>

        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>
