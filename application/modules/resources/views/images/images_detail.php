<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="page-wrapper operations_manuals">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Resources / Image Library</h2>
                <hr>
            </div>
        </div>

        <form id="Country" action="<?=current_url();?>" method="get">
            <div class="row">
                <div class="col-md-12">
                    <div class="listdata">
                        <div class="item-category">
                            <a href="<?=site_url('resources/images');?>">
                                <div class="sub">Image Library</div>
                            </a>
                            <?php
                            $this->db->from('branch');
                            $this->db->where('branchID', $this->uri->segment(4));
                            $result = $this->db->get();
                            foreach ($result->result_array() as $value) {
                            ?>
                            <div class="sub active"><?=$value['title'];?></div>
                            <?php } ?>
                            <div class="select">
                                <select class="form-control" name="countryID"
                                    change-country="<?=site_url('home/country_ajax');?>">
                                    <option value="" hidden>-- Country --</option>
                                    <?php
                                    $this->db->from('country');
                                    $country = $this->db->get();
                                    foreach ($country->result() as $value) {
                                    ?>
                                    <?php if ($value->countryID==$this->input->get('countryID')) { ?>
                                    <option value="<?=$value->countryID;?>" selected><?=$value->title;?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$value->countryID;?>"><?=$value->title;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="select">
                                <select class="form-control" name="zoneID"
                                    change-zone="<?=site_url('home/restaurant_ajax');?>" id="respon-zone">
                                    <option value="" hidden>-- City --</option>
                                    <?php
                                    $this->db->where('countryID', $this->input->get('countryID'));
                                    $this->db->from('country_zone');
                                    $country_zone = $this->db->get();
                                    foreach ($country_zone->result() as $value) {
                                    ?>
                                    <?php if ($value->zoneID==$this->input->get('zoneID')) { ?>
                                    <option value="<?=$value->zoneID;?>" selected><?=$value->title;?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$value->zoneID;?>"><?=$value->title;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="select">
                                <select class="form-control" name="restaurantID" id="respon-restaurant">
                                    <option value="" hidden>-- Restaurant --</option>
                                    <?php
                                    $this->db->where('branchID', $this->input->get('branchID'));
                                    $this->db->where('zoneID', $this->input->get('zoneID'));
                                    $this->db->from('restaurant');
                                    $restaurant = $this->db->get();
                                    foreach ($restaurant->result() as $value) {
                                    ?>
                                    <?php if ($value->restaurantID==$this->input->get('restaurantID')) { ?>
                                    <option value="<?=$value->restaurantID;?>" selected><?=$value->title;?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$value->restaurantID;?>"><?=$value->title;?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <input type="hidden" id="branchID" name="branchID" value="<?=$this->uri->segment(4);?>">

                        </div>
                    </div>
                </div>
            </div>
        </form>

        <?php if ($this->session->sess_login['type']=='Corporate') { ?>
        <?php if (!empty($this->input->get('restaurantID'))) { ?>
        <!-- <form action="<?=current_url();?>" method="post" enctype="multipart/form-data"> -->
        <form action="<?=site_url('resources/images/multiple_upload');?>" method="post" enctype="multipart/form-data">
            <div style="background: #556080;padding: 15px;">
                <div class="row">
                    <div class="col-8">
                        <div class="custom-file" id="customFile" lang="es">
                            <input type="file" class="custom-file-input" name="filename[]" accept="image/*" multiple>
                            <label class="custom-file-label" for="exampleInputFile">
                                Select file...
                            </label>
                        </div>
                    </div>
                    <div class="col-4">
                        <input type="hidden" id="countryID" name="countryID"
                            value="<?=$this->input->get('countryID');?>">
                        <input type="hidden" id="zoneID" name="zoneID" value="<?=$this->input->get('zoneID');?>">
                        <input type="hidden" id="branchID" name="branchID" value="<?=$this->input->get('branchID');?>">
                        <input type="hidden" id="restaurantID" name="restaurantID"
                            value="<?=$this->input->get('restaurantID');?>">
                        <button type="submit" class="btn btn-success"><i class="fas fa-cloud-upload-alt"></i> UPLOAD</button>
                    </div>
                </div>
            </div>
        </form>
        <?php } ?>
        <?php } ?>

        <div class="row listdata">
            <?php
            $this->db->where('countryID', $this->input->get('countryID'));
            $this->db->where('zoneID', $this->input->get('zoneID'));
            $this->db->where('branchID', $this->input->get('branchID'));
            $this->db->where('restaurantID', $this->input->get('restaurantID'));
            $this->db->from('resources_images');
            $resources_images = $this->db->get()->result();
            if(!empty($resources_images)){
            foreach ($resources_images as $value) {
            ?>
            <div class="col-lg-2 col-md-3 col-sm-4 my-4">
                <a href="<?=base_url('uploads/resources/images/'.$value->files);?>" data-fancybox="images"
                    data-caption="resources">
                    <div class="item-image boder">
                        <img src="<?=base_url('uploads/resources/images/'.$value->files);?>" alt="" class="img"
                            style="padding: 0;">
                    </div>
                </a>
                <div class="text-center">
                    <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                    <a href="javascript:void(0);"
                        click-delete="<?=site_url('resources/images/delete/'.$value->imagesID);?>">
                        <button type="button" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                    </a>
                    <?php  } ?>
                    <a href="<?=base_url('uploads/resources/images/'.$value->files);?>" download>
                        <button type="button" class="btn btn-success"><i class="fas fa-download"></i></button>
                    </a>
                </div>
            </div>
            <?php  } }else{ ?>
            <div class="col-12 mt-3">
                <div class="alert alert-warning text-center">
                    <strong>! </strong> Please select from drop down list.

                </div>
            </div>
            <?php  } ?>
            <!-- Paginate -->
            <!-- <div class="mt-4" id="pagination"></div> -->
        </div>

        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>
