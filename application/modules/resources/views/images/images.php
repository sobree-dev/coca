<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="page-wrapper operations_manuals">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Resources</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <a href="<?=site_url('resources/images');?>">
                            <div class="sub active">Image Library</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row listdata">
            <div class="offset-md-2 col-md-8">
                <div class="row">
                    <?php
                    $branch = $this->db->get('branch');
                    foreach ($branch->result() as $val) {
                    ?>
                    <div class="col-md-4 mb-4">
                        <a href="<?=site_url('resources/images/detail/'.$val->branchID);?>">
                            <div class="item-image">
                                <img src="<?=base_url('uploads/branch/'.$val->image);?>" alt="" class="img">
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php
        $base =  base_url('image/resources');
        $files = glob("image/resources/*.*");
        for ($i=1; $i<count($files); $i++){
        ?>
        <!-- <img src="<?=base_url($files[$i]);?>" alt="" class="img"> -->
        <?php } ?>

        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
</div>