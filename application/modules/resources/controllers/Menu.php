<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBrecord');
        
        $this->id = end($this->uri->segment_array());
        $this->table = 'resources_menu';

	}

    private function seo()
	{
		$title          = "Resources / Menu";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('resources/menu/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
        $sess_data['id'] = null;
        $sess_data['link'] = current_url();
        if ($this->session->data_search['QUERY_STRING']) {
            $sess_data['link'] = current_url().'?'.$this->session->data_search['QUERY_STRING'];
        }
        $this->session->set_userdata('resources_menu',$sess_data);
    }
    
    private function upload_image($file, $path) {
		$config['upload_path'] = $path;
		$config['allowed_types'] = '*';
		$config['max_size'] = '*';
		$config['max_width']  = '*';
		$config['max_height']  = '*';
		$config['encrypt_name']  = TRUE;
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($file)) {
			return array('file_name' => false); // ถ้าอัพโหลดไม่ได้ ไม่สามารถเรียกดูข้อมูลไฟล์ที่อัพได้
		}else{
            $file_name = $this->upload->data();  // ถ้าอัพโหลดได้ เราสามารถเรียกดูข้อมูลไฟล์ที่อัพได้
            return $file_name['file_name'];
		}
	}

	public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'resources',
            'header'  => 'header',
            'content' => 'menu/index',
            'footer'  => 'footer',
            'function'=>  array('resources'),
        );
        $this->load->view('template/body', $data);
    }

    public function detail()
	{
        $sess_data = array(
            'countryID' => $this->input->get('countryID'),
            'zoneID' => $this->input->get('zoneID'),
            'restaurantID' => $this->input->get('restaurantID'),
            'branchID' => $this->input->get('branchID'),
            'QUERY_STRING' => $_SERVER['QUERY_STRING'],
        );
        $this->session->set_userdata('data_search',$sess_data);
        
        $this->thisURL();

        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'resources',
            'header'  => 'header',
            'content' => 'menu/detail',
            'footer'  => 'footer',
            'function'=>  array('resources'),
        );

        // DBrecord //
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'resources',
            'header'  => 'header',
            'content' => 'menu/form',
            'footer'  => 'footer',
            'function'=>  array('resources'),
        );
        // DBrecord //
        $DBrecord['id'] = array('menuID' =>  $this->id);
        $DBrecord['order_by'] = array('menuID' => 'DESC');
        $DBrecord['table'] = $this->table;
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['title'] = $input['title'];
        $value['detail'] = $input['detail'];
        $value['countryID'] = $this->session->data_search['countryID'];
        $value['zoneID'] = $this->session->data_search['zoneID'];
        $value['branchID'] = $this->session->data_search['branchID'];
        $value['restaurantID'] = $this->session->data_search['restaurantID'];

        if ($input['menuID']==null) {
            $value['createDate'] = date('Y-m-d H:i:s');
            $value['createBy'] = $this->session->sess_login['usersID'];
        } else {
            $value['updateDate'] = date('Y-m-d H:i:s');
            $value['updateBy'] = $this->session->sess_login['usersID'];
        }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);

        $value['file_doc'] = $input['file_doc_hid'];
		if (isset($_FILES['file_doc']['name']) && !empty($_FILES['file_doc']['name'])) {
            $value['file_doc'] = $this->upload_image('file_doc', './uploads/resources/menu/');
        }

        $value['image'] = $input['file_img_hid'];
		if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/resources/menu/');
        }
        
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->insert($DBrecord);
        // // DBrecord //
        redirect( $this->session->resources_menu['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        
        $value['file_doc'] = $input['file_doc_hid'];
        if (isset($_FILES['file_doc']['name']) && !empty($_FILES['file_doc']['name'])) {
            $value['file_doc'] = $this->upload_image('file_doc', './uploads/resources/menu/');
            if ($value['file_doc']) {
                unlink('./uploads/resources/menu/'.$input['file_doc_hid']);
            }
        }

        $value['image'] = $input['file_img_hid'];
        if (isset($_FILES['file_img']['name']) && !empty($_FILES['file_img']['name'])) {
            $value['image'] = $this->upload_image('file_img', './uploads/resources/menu/');
            if ($value['image']) {
                unlink('./uploads/resources/menu/'.$input['file_img_hid']);
            }
        }

        $DBrecord['id'] = array('menuID'=>$input['menuID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = $this->table;
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->resources_menu['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('menuID' =>  end($this->uri->segment_array()));
        $DBrecord['table'] = $this->table;

        $result = $this->DBrecord->get_first($DBrecord);
        if ($result->file_doc) {
            unlink('./uploads/resources/menu/'.$result->file_doc);
        }

        if ($result->image) {
            unlink('./uploads/resources/menu/'.$result->image);
        }

        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->resources_menu['link'], 'refresh');
	}
    
}