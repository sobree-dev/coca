<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_sub extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('DBrecord');
	}

    private function seo()
	{
		$title          = "setting / Menu sub";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

    private function SiteURL($SiteURL)
	{
		$SiteURL = site_url('setting/menu_sub/'.$SiteURL);
        return $SiteURL;
	}
    
    private function thisURL()
	{
		$sess_data = array(
            'id' => end($this->uri->segment_array()),
            'link' => current_url()
        );
        $this->session->set_userdata('thisURL',$sess_data);
	}

    public function index()
	{
        $this->thisURL();
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'setting',
            'header'  => 'header',
            'content' => 'setting/menu_sub/index',
            'footer'  => 'footer',
            'function'=>  array('setting'),
        );
        // DBrecord //
        $DBrecord['table'] = 'menu_sub';
        $DBrecord['id'] = array('menuID' => $this->session->thisURL['id']);
        $data['result'] = $this->DBrecord->get_result($DBrecord);
        // DBrecord //
        $data['Urladd'] = $this->SiteURL('form/add');
        $data['Urledit'] = $this->SiteURL('form');
        $data['Urldelete'] = $this->SiteURL('delete');
        $this->load->view('template/body', $data);
    }
    
    public function form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'setting',
            'header'  => 'header',
            'content' => 'setting/menu_sub/form',
            'footer'  => 'footer',
            'function'=>  array('setting'),
        );
        // DBrecord //
        $DBrecord['id'] = array('subID' => end($this->uri->segment_array()));
        $DBrecord['table'] = 'menu_sub';
        $data['result'] = $this->DBrecord->get_first($DBrecord);
        // DBrecord //
        if (end($this->uri->segment_array())=='add') {
            $data['Urlform'] = $this->SiteURL('create');
        } else {
            $data['Urlform'] = $this->SiteURL('update');
        }
        
        $this->load->view('template/body', $data);
    }
    
    public function _build_data($input)
	{
        $value['label'] = $input['label'];
        $value['menuID'] = $input['menuID'];

        // if ($input['menuID']==null) {
        //     $value['createDate'] = date('Y-m-d H:i:s');
        //     $value['createBy'] = $this->session->sess_login['usersID'];
        // } else {
        //     $value['updateDate'] = date('Y-m-d H:i:s');
        //     $value['updateBy'] = $this->session->sess_login['usersID'];
        // }
        return $value;
    }

	public function create()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'menu_sub';
        $this->DBrecord->insert($DBrecord);
        // DBrecord //
        redirect( $this->session->thisURL['link'], 'refresh');
    }
    
    public function update()
	{
        // DBrecord //
        $input = $this->input->post();
        $value = $this->_build_data($input);
        $DBrecord['id'] = array('subID' => $input['subID']);
        $DBrecord['value'] = $value;
        $DBrecord['table'] = 'menu_sub';
        $this->DBrecord->update($DBrecord);
        // DBrecord //
        redirect( $this->session->thisURL['link'], 'refresh');
    }
    
    public function delete()
	{
        // DBrecord //
        $DBrecord['id'] = array('subID' => end($this->uri->segment_array()));
        $DBrecord['table'] = 'menu_sub';
        $this->DBrecord->delete($DBrecord);
        // DBrecord //
        redirect( $this->session->thisURL['link'], 'refresh');
	}
    
}