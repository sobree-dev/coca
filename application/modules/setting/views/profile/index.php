<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Profile</h2>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="listdata">
                    <div class="item-category">
                        <div class="sub active">Profile</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="text-right">
                    <a href="<?=$Urladd;?>">
                        <!-- <button type="button" class="btn btn-success"><i class="fas fa-plus"></i> Add</button> -->
                    </a>
                </div>

                <div class="listdata">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Role</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($result as $val) {
                            ?>
                            <tr>
                                <td>
                                    <div class="tr-col"><?=$val->fullname;?></div>
                                </td>
                                <td>
                                    <div class="tr-col"><?=$val->email;?></div>
                                </td>
                                <td>
                                    <div class="tr-col"><?=$val->username;?></div>
                                </td>
                                <td>
                                    <?php $val_group = $this->db->get_where('users_group', array('groupID' => $val->groupID))->row();?>
                                    <div class="tr-col"><?=$val_group->title;?></div>
                                </td>
                                <td>
                                    <div class="tr-col text-right">
                                        <a href="<?=$Urledit.'/'.$val->usersID;?>">
                                            <button type="button" class="btn btn-warning">Edit</button>
                                        </a>
                                        <a href="javascript:void(0);"
                                            click-delete="<?=$Urldelete.'/'.$val->usersID;?>">
                                            <button type="button" class="btn btn-danger">Delete</button>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>