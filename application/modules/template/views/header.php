<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header" style="text-align: center;padding: 0;">
            <a class="navbar-brand" href="<?=site_url();?>">
                <!-- <b>m</b>
                <span>ango tree</span> -->
                <img src="<?=base_url('image/logo/logo.png');?>" alt="">
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <?php
            $branch = modules::run('userspermis/branch');
            $branchs =  $this->db->get_where('branch', array('branchID' => $branch->branchID))->row();
            ?>
            <?php if ($this->session->sess_login['type']=='Restaurant') { ?>
            <img src="<?=base_url('uploads/branch/'.$branchs->image);?>" alt="" style="height: 35px;padding: 5px;">
            <?php }else{ ?>
            <?php
            $branch2 = $this->db->get('branch');
            foreach ($branch2->result() as $branchs2) {
            ?>
            <img src="<?=base_url('uploads/branch/'.$branchs2->image);?>" alt="" style="height: 35px;padding: 5px;">
            <?php } ?>
            <?php } ?>

            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up waves-effect waves-dark"
                        href="javascript:void(0)"><i class="fas fa-align-left"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down waves-effect waves-dark"
                        href="javascript:void(0)"><i class="fas fa-align-left"></i></a> </li>
                <li class="nav-item hidden-sm-down"></li>
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <!-- Profile -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle waves-effect waves-dark" href="javascript:void(0);" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <?php
                        $get_users = modules::run('userspermis/get_users');
                        if ($get_users->image) {
                        ?>
                        <img src="<?=base_url('/uploads/users/'.$get_users->image);?>" alt="user" class="profile-pic" />
                        <?php }else{ ?>
                        <img src="<?=base_url('image/user.png');?>" alt="user" class="profile-pic" />
                        <?php } ?>
                        <div class="name">
                            <?php
                            $users_group = modules::run('userspermis/users_group');
                            ?>
                            <p>Welcome, <?=$this->session->sess_login['firstname'];?> <i class="fas fa-angle-down"></i></p>
                            <span><?=$users_group->title;?></span>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img">
                                    <?php if ($get_users->image) { ?>
                                        <img src="<?=base_url('/uploads/users/'.$get_users->image);?>" alt="user"/>
                                    <?php }else{ ?>
                                        <img src="<?=base_url('image/user.png');?>" alt="user">
                                    <?php } ?>
                                    </div>
                                    <div class="u-text">
                                        <h4><?=$this->session->sess_login['firstname'];?></h4>
                                        <p class="text-muted"><?=$this->session->sess_login['email'];?></p>
                                    </div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=site_url('setting/profile/form/'.$this->session->sess_login['usersID']);?>"><i class="fas fa-user-cog"></i> Account Setting</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?=site_url('home/logout');?>"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- <li class="user-profile"> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);"
                        aria-expanded="false"><img src="<?=base_url('image/user.png');?>" alt="user" /><span
                            class="hide-menu"><?=$this->session->sess_login['firstname'];?></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="javascript:void()">My Profile </a></li>
                        <li><a href="javascript:void()">Account Setting</a></li>
                        <li><a href="<?=site_url('home/logout');?>">Logout</a></li>
                    </ul>
                </li>
                <li class="nav-devider"></li> -->
                <li class="nav-small-cap">MENU</li>
                <li class="<?php if ($menu=='home') {echo 'active';} ?>">
                    <a class="waves-effect waves-dark" href="<?=site_url('home');?>">
                        <i class="fas fa-tachometer-alt"></i><span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li class="<?php if ($menu=='operations') {echo 'active';} ?>">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false">
                        <i class="fab fa-codepen"></i><span class="hide-menu">Operations</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="<?=site_url('operations/recipe');?>">Recipe Center</a>
                        </li>
                        <li class="">
                            <a href="<?=site_url('operations/manuals');?>">Manuals & Form</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($menu=='marketing') {echo 'active';} ?>">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false">
                        <i class="far fa-circle"></i><span class="hide-menu">Marketing</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="<?=site_url('marketing/templates');?>">Marketing Templates</a>
                        </li>
                        <li>
                            <a href="<?=site_url('marketing/coca');?>">COCA Branding Toolkit</a>
                        </li>
                        <li>
                            <a href="<?=site_url('marketing/mangotree ');?>">Mango Tree Branding Toolkit</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($menu=='training') {echo 'active';} ?>">
                    <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false">
                        <i class="fas fa-tasks"></i><span class="hide-menu">Training</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="<?=site_url('training/orientation');?>">Orientation</a>
                        </li>
                        <li>
                            <a href="<?=site_url('training/brandoverview');?>">Brand Overview</a>
                        </li>
                        <!-- <li>
                            <a href="javascript:void(0);">Management & Accounting</a>
                        </li> -->
                        <li>
                            <a href="<?=site_url('training/brandoverview/index/service');?>">Front of House</a>
                        </li>
                        <li>
                            <a href="<?=site_url('training/brandoverview/index/foodbeverage');?>">Back of House</a>
                        </li>
                        <!-- <li>
                            <a href="javascript:void(0);">Marketing</a>
                        </li> -->
                    </ul>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false"><i
                            class="far fa-circle"></i><span class="hide-menu">Report</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="javascript:void(0);">Corporate Audit (corporate)</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Monthly Audit</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Daily checklist</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Monthly report</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($menu=='resources') {echo 'active';} ?>"> <a
                        class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false">
                        <i class="fab fa-buffer"></i><span class="hide-menu">Resources</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="<?=site_url('resources/images');?>">Image Library</a>
                        </li>
                        <li>
                            <a href="<?=site_url('resources/menu');?>">Menu</a>
                        </li>
                        <li>
                            <a href="<?=site_url('resources/promotions');?>">Promotions</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($menu=='news') {echo 'active';} ?>"> <a
                        class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false"><i
                            class="far fa-circle"></i><span class="hide-menu">News & Alerts</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="<?=site_url('news/newsletter');?>">Newsletter</a>
                        </li>
                        <li>
                            <a href="<?=site_url('news/promotions');?>">New Promotions</a>
                        </li>
                        <li>
                            <a href="<?=site_url('news/events');?>">News & Events</a>
                        </li>
                    </ul>
                </li>
                <li class="<?php if ($menu=='setting') {echo 'active';} ?>"> <a
                        class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false">
                        <i class="fas fa-user-cog"></i><span class="hide-menu">Setting</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li>
                            <a href="<?=site_url('setting/profile/form/'.$this->session->sess_login['usersID']);?>">Profile</a>
                        </li>
                        <!-- <li>
                            <a href="<?=site_url('setting/menu');?>">Menu System</a>
                        </li> -->
                    </ul>
                </li>
                <?php
                $users_group = modules::run('userspermis/users_group');
                if ($users_group->title=='Restaurant Manager'||$this->session->sess_login['type']=='Corporate'||$this->session->sess_login['type']=='Franchisee') {
                ?>
                <li class="<?php if ($menu=='country') {echo 'active';} ?>"> <a
                        class="has-arrow waves-effect waves-dark" href="javascript:void(0);" aria-expanded="false">
                        <i class="fas fa-globe-americas"></i><span class="hide-menu"> Control Panel</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <?php if ($this->session->sess_login['type']=='Corporate') { ?>
                        <li>
                            <a href="<?=site_url('country/access');?>">User Role</a>
                        </li>
                        <li>
                            <a href="<?=site_url('country/branch');?>">Brand</a>
                        </li>   
                        <li>
                            <a href="<?=site_url('country');?>">Country</a>
                        </li>
                        <li>
                            <a href="<?=site_url('country/restaurant');?>">Restaurant</a>
                        </li>
                        <li>
                            <a href="<?=site_url('country/franchisee');?>">Franchisee</a>
                        </li>
                        <?php } ?>
                        <?php if ($this->session->sess_login['type']=='Restaurant') { ?>
                        <li>
                            <a href="<?=site_url('country/restaurant');?>">Restaurant</a>
                        </li>
                        <?php } ?>
                        <?php if ($this->session->sess_login['type']=='Franchisee') { ?>
                            <li>
                            <a href="<?=site_url('country/franchisee');?>">Franchisee</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                <li class="nav-devider"></li>
                <li class="nav-small-cap">ADMIN SYTEM</li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->