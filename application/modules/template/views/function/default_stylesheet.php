<link rel="stylesheet" href="<?=base_url('plugin/admin-theme/fontawesome-free-5/css/all.css');?>" rel="stylesheet">
<link rel="stylesheet" href="<?=base_url('class/css/app.css');?>">
<link rel="stylesheet" href="<?=base_url('plugin/bootstrap/bootstrap.min.css');?>">
<script type="text/javascript" src="<?=base_url('plugin/bootstrap/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('plugin/bootstrap/popper.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('plugin/bootstrap/bootstrap.min.js');?>"></script>
<link rel="stylesheet" href="<?=base_url('plugin/admin-theme/css/style.css');?>" rel="stylesheet">

<!-- You can change the theme colors from here -->
<link rel="stylesheet" href="<?=base_url('plugin/admin-theme/css/default-theme.css');?>" id="theme" rel="stylesheet">

<!-- loading -->
<link rel="stylesheet" href="<?=base_url('plugin/loading/jquery.loading.css'); ?>">

<!-- sweetalert2 -->
<link rel="stylesheet" href="<?=base_url('plugin/sweetalert2/sweetalert2.min.css'); ?>">

<!-- select2 -->
<link rel="stylesheet" href="<?=base_url('plugin/select2/bootstrap-select.min.css'); ?>">
<!-- <link rel="stylesheet" href="<?=base_url('plugin/select2/select2-bootstrap4.css'); ?>">
<link rel="stylesheet" href="<?=base_url('plugin/select2/select2.min.css'); ?>"> -->

<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url('plugin/DataTables/datatables.css'); ?>">
<link rel="stylesheet" href="<?=base_url('plugin/DataTables/Responsive-2.2.3/css/responsive.dataTables.min.css'); ?>">

<!-- jasny-bootstrap -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
