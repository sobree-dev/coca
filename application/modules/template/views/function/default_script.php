<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?=base_url('plugin/admin-theme/js/perfect-scrollbar.jquery.min.js');?>"></script>
<!--Wave Effects -->
<script src="<?=base_url('plugin/admin-theme/js/waves.js');?>"></script>
<!--Menu sidebar -->
<script src="<?=base_url('plugin/admin-theme/js/sidebarmenu.js');?>"></script>
<!--stickey kit -->
<script src="<?=base_url('plugin/admin-theme/js/sticky-kit.min.js');?>"></script>
<script src="<?=base_url('plugin/admin-theme/js/jquery.sparkline.min.js');?>"></script>
<!--Custom JavaScript -->
<script src="<?=base_url('plugin/admin-theme/js/custom.min.js');?>"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<!-- <script src="<?=base_url('plugin/admin-theme/js/jQuery.style.switcher.js');?>"></script> -->
<!-- jasny-bootstrap -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>


<script type='text/javascript'>
    $(document).ready(function () {
        // alert( $('#loadAjax_data').attr('data-link') );

        // Detect pagination click
        $('#pagination').on('click', 'a', function (e) {
            if ($(this).attr('href') != 'javascript:void(0);') {
                e.preventDefault();
                var pageno = $(this).attr('data-ci-pagination-page');
                loadPagination(pageno);
                $("body").loading();
            }
        });

        if ($('#loadAjax_data').attr('data-link')) {
            loadPagination(0);
        }

        // Load pagination
        function loadPagination(pagno) {
            var datalink = $('#loadAjax_data').attr('data-link');
            var dataid = $('#loadAjax_data').attr('data-id');

            $.ajax({
                url: datalink + '/' + pagno,
                type: 'get',
                data: {
                    Url: datalink,
                    whreID: dataid
                },
                dataType: 'json',
                success: function (response) {
                    $("body").loading('stop');
                    // console.log(response.link);
                    $('#loadAjax_data').html(response.loadRecordAjax);
                    $('#pagination').html(response.pagination);
                }
            });
        }
    });
</script>

<!-- loading -->
<script type="text/javascript" src="<?=base_url('plugin/loading/jquery.loading.min.js');?>"></script>

<!-- sweetalert2 -->
<script type="text/javascript" src="<?=base_url('plugin/sweetalert2/sweetalert2.min.js');?>"></script>
<script>
    $('a[click-delete]').click(function (e) {
        var link = $(this).attr('click-delete');
        Swal.fire({
            title: 'Are you sure?',
            text: "If deleted, cannot be restored !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(link);
            }
        })
    });
</script>

<script>
    $('a[click-deactivate]').click(function (e) {
        var link = $(this).attr('click-deactivate');
        Swal.fire({
            title: 'Are you sure ?',
            text: "If deactivate, cannot be restored !",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, deactivate it!'
        }).then((result) => {
            if (result.value) {
                window.location.replace(link);
            }
        })
    });
</script>

<!-- select2 -->
<script type="text/javascript" src="<?=base_url('plugin/select2/bootstrap-select.min.js');?>"></script>
<!-- <script type="text/javascript" src="<?=base_url('plugin/select2/select2.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('plugin/select2/script.js');?>"></script> -->
<script>
    $(function () {
        // $('select').selectpicker();
    });
</script>

<!-- DataTables -->
<script src="<?=base_url('plugin/DataTables/datatables.js');?>"></script>
<script src="<?=base_url('plugin/DataTables/Responsive-2.2.3/js/dataTables.responsive.js');?>"></script>
<script>
    $(document).ready(function () {
        // $('table').DataTable({
        //     // responsive: true,
        //     // stateSave: true,
        // });
    });
</script>

<!-- dottext -->
<script>
    $('.dot-ellipsis').css('opacity', '0');

    function dottext() {
        $('.dot-ellipsis').css('opacity', '1');
    }
    setTimeout(dottext, 500);
</script>
<script type="text/javascript" src="<?=base_url('plugin/dottext/dottext.js');?>"></script>


<!-- validate -->
<script type="text/javascript" src="<?=base_url('plugin/validate/jquery.validate.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('plugin/validate/additional-methods.min.js');?>"></script>
<script>
    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than {0}');

    $("form").validate({
        rules: {
            image: {
                extension: "jpeg|jpg|png|gif",
                filesize: 3000000, // 9MB
            },
        },
        messages: {
            image: {
                filesize: "upload image max size 2MB",
                extension: "upload image type (jpeg, jpg, png, gif)",
            }
        },
        errorElement: "span",
    });
</script>


<script src='https://cdn.tiny.cloud/1/6seaqq9ivjpqs0k1gkxd3ap7cf0wdkpyekq01l2x0fsyzqgp/tinymce/5/tinymce.min.js'
    referrerpolicy="origin">
</script>
<script>
    tinymce.init({
        selector: "textarea", // change this value according to your HTML
    });
</script>