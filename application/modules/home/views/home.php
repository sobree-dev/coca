<!-- Start Page Content -->
<div id="loadding_page">
    <div class="logos">
        <img src="<?=base_url('image/logo/logo.png');?>" alt="">
    </div>
</div>

<div class="content-top dashboard">
    <img src="<?=base_url('image/bg/image.JPG');?>" alt="" class="wfull banner">

    <div class="bg-gray">
        <div class="container-fluid-default">
            <div class="row">
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <a href="<?=site_url('operations/recipe');?>">
                        <div class="bloge-menu disable">
                            <img src="<?=base_url('image/dashboard/1.jpg');?>" alt="" class="wfull">
                            <div class="text">
                                <span>Operations</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <a href="<?=site_url('marketing/templates');?>">
                        <div class="bloge-menu disable">
                            <img src="<?=base_url('image/dashboard/2.jpg');?>" alt="" class="wfull">
                            <div class="text">
                                <span>Marketing</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <a href="<?=site_url('training/orientation');?>">
                        <div class="bloge-menu">
                            <img src="<?=base_url('image/dashboard/3.jpg');?>" alt="" class="wfull">
                            <div class="text">
                                <span>Training</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <a href="javascript:void(0);">
                        <div class="bloge-menu disable">
                            <img src="<?=base_url('image/dashboard/4.jpg');?>" alt="" class="wfull">
                            <div class="text">
                                <span>Reports</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <a href="<?=site_url('resources/images');?>">
                        <div class="bloge-menu">
                            <img src="<?=base_url('image/dashboard/5.jpg');?>" alt="" class="wfull">
                            <div class="text">
                                <span>Resources</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-6 col-md-4 col-lg-2">
                    <a href="<?=site_url('news/newsletter');?>">
                        <div class="bloge-menu disable">
                            <img src="<?=base_url('image/dashboard/6.jpg');?>" alt="" class="wfull">
                            <div class="text">
                                <span>News & Alerts</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;" id="hidden-content">
        <h1>coming soon</h1>
    </div>
    <div class="container-fluid-default">
        <div class="row mt-4">
            <div class="col-lg-4">
                <div class="row">
                    <?php
                    $text = array('','New Recipe','Corporate News','Reports to Complete');
                    for ($i=1; $i < 4; $i++) {  ?>
                    <div class="col-lg-12">
                        <a data-fancybox data-src="#hidden-content" href="javascript:;">
                            <div class="new-list">
                                <div class="left" style="font-size: 15px;">
                                    <img src="<?=base_url('image/NewDashboard/'.$i.'.png');?>" alt="" class="wfull" style="width: 50px;">
                                    <div class="text text-center">
                                        <span><?=$text[$i];?></span>
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="text">
                                        <br>
                                        <p class="int" style="font-size: 13px;">coming soon</p>
                                        <!-- <p><span class="badge badge-danger">Files</span></p>
                                        <p>Update</p> -->
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog-training">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="head"><b>Training</b></h3>
                        </div>
                        <?php
                        $this->db->order_by('id', 'RANDOM');
                        $this->db->limit(4);
                        $training_group = $this->db->get('training_group');                         
                        foreach ($training_group->result() as $key => $val) {
                        $training_answer = modules::run('userspermis/training_answer', $val->id);
                        ?>
                        <div class="col-lg-12 col-12">
                            <a href="<?=site_url('training/orientation');?>">
                                <div class="blog">
                                    <div class="left">
                                        <span><?=$val->title;?></span>
                                    </div>
                                    <div class="right">
                                        <div class="circlechart" data-percentage="75"><?php if($training_answer){echo $training_answer.' / ';}echo $val->total;?></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                        <div class="col-lg-12 col-12">
                            <div class="text-center">
                                <a href="<?=site_url('training/orientation');?>">
                                    <span style="background: rgb(240, 146, 40);color: #fff;padding: 5px 18px;border-radius: 18px;">View More</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog-QuickLinks">
                    <div class="row">
                        <div class="col-lg-12 head">
                            <h3 class="ti"><b>Quick Links</b></h3>
                        </div>
                        <?php 
                        // $text = array('','Recipe Center','Manuals & Forms','Marketing Toolkits','Image Library');
                        $text = array('','Image Library','Menu');
                        $link = array('','images','menu');
                        for ($i=1; $i < 3; $i++) {  ?>
                        <div class="col-lg-6 col-6">
                            <a href="<?=site_url('resources/'.$link[$i]);?>">
                                <div class="left">
                                    <div>
                                        <img src="<?=base_url('image/QuickLinks/4.png');?>" alt="" class="wfull">
                                    </div>
                                    <div class="text">
                                        <span><?=$text[$i];?></span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="blog-training">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="head"><b>Report</b></h3>
                        </div>
                        <div class="col-lg-12 text-center">
                        <h2>coming soon</h2>
                        </div>
                        <!-- <div class="col-4">
                            <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                <button type="button" class="btn btn-light wfull">Monthly Audit</button>
                            </a>
                        </div>
                        <div class="col-4">
                            <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                <button type="button" class="btn btn-light wfull">Monthly Report</button>
                            </a>
                        </div>
                        <div class="col-4">
                            <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                <button type="button" class="btn btn-danger wfull scale">Daily checklist</button>
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

        <!-- End PAge Content -->
    </div>
</div>