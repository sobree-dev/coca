<!-- ============================================================== -->
<div class="page-wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">
                <h2>Corporate</h2>
                <hr><br>
                <?php
                $updateid = isset($result->id) ? $result->id : '';
                if($updateid == ''){
                    echo form_open('corporate/training/orientation_insert');
                } else {
                    echo form_open('corporate/training/orientation_update/'.base64_encode($updateid));
                }
                ?>
                <form action="<?=site_url('corporate/training/orientation_insert');?>" method="POST" role="form" class="form-horizontal">
                    <div class="form-group">
                        <div class="row">
                            <label for="title" class="col-md-2 control-label">title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="title" name="title" value="<?=isset($result->title) ? $result->title : '';?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="title" class="col-md-2 control-label">category</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="category" name="category" value="<?=isset($result->category) ? $result->category : '';?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="total" class="col-md-2 control-label">total</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="total" name="total" value="<?=isset($result->total) ? $result->total : '';?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="must" class="col-md-2 control-label">must</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="must" name="must" value="<?=isset($result->must) ? $result->must : '';?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="offset-md-2 col-md-10">
                                <button type="submit" class="btn btn-info">SAVE</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- ============================================================== -->
    </div>
</div>