<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('grocery_CRUD');
		$this->load->helper(array('form', 'url', 'file'));
	}

	public function _example_output($output = null)
	{
		$this->load->view('admin',(array)$output);
	}

	// public function index()
	// {
	// 	$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	// }

	public function adminlogin()
	{
		if ($this->input->get('type')=='login') {

			$post_password = base64_encode($this->input->post('admin_pass').'#TK@@!');
	
			$result0 = $this->db->get_where('users', array('username' => $this->input->post('admin_user'), 'password' => $post_password));
			$result = $result0->row();
			if($result0->num_rows()<1){
				header ('Content-type: text/html; charset=utf-8');
				print "<script type=\"text/javascript\">alert('ชื่อผุ้ใช้หรือรหัสผ่านไม่ถูกต้อง');</script>";
				redirect(base_url().'admin', 'refresh');
			}else {
				$users_groups = $this->db->get_where('users_groups', array('user_id' => $result->id))->row();
				$sess_array['admin_id'] = $result->id;
				$sess_array['username'] = $result->username;
				if ($result->position=='superadmin') {
					$sess_array['group_id'] = $result->position;
				} else {
					$sess_array['group_id'] = $users_groups->group_id;
				}
				$this->session->set_userdata('sess_admin', $sess_array);
				redirect(base_url().'admin/home/?ac=hom', 'refresh');
			}
		} else {
			$this->load->view('admin_login');
		}
	}

	public function adminlogout()
	{
		$this->session->sess_destroy();
		redirect(base_url('admin'), 'refresh');
	}


	

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private function is_function($table)
	{
		if(isset($table)){
			$this->session->set_userdata('table', $table);
		}
		$tables = $this->session->userdata('table');
		
		$crud = new grocery_CRUD();
		$crud->set_theme('bootstrap');
		$crud->set_table($tables);
		$display_as = array(
			'sort' => 'ลำดับ',
			'title_lang1' => 'ชื่อ(EN)',
			'title_lang2' => 'ชื่อ(TH)',
			'subject_lang1' => 'รายละเอียดย่อย(EN)',
			'subject_lang2' => 'รายละเอียดย่อย(TH)',
			'detail_lang1' => 'เนื้อหา(EN)',
			'detail_lang2' => 'เนื้อหา(TH)',
			'is_date' => 'วันที่/เวลา',
		);
		$crud->display_as($display_as);
		// ------------------- display_as -----------------//
		$crud->field_type('is_delete', 'hidden');
		$crud->field_type('is_date', 'hidden', date('Y-m-d H:i:s'));
		$crud->order_by('sort', 'ASC');
		return $crud;
	}

	public function banner_home()
	{
			$crud = $this->is_function('banner_home');
			$crud->set_subject('Banner Home');
			$crud->columns(array('sort','image','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$display_as = array(
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image','uploads/banner_home');
			// ------------------- field_upload -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}

	public function banner_other()
	{
			$crud = $this->is_function('banner_other');
			$crud->set_subject('Banner Other');
			$crud->columns(array('sort','image','page','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$display_as = array(
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$page = array(
				'about' => 'about',
				'product' => 'product',
				'news' => 'news',
				'faq' => 'faq',
				'contact' => 'contact',
			);
			$crud->field_type('page', 'dropdown', $page);

			$crud->set_field_upload('image','uploads/banner_other');
			// ------------------- field_upload -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}

	public function home_about()
	{
			$crud = $this->is_function('home_about');
			$crud->set_subject('About');
			$crud->columns(array('sort','image','is_date'));
			$crud->required_fields('image');
			// ------------------- is_function -----------------//

			$display_as = array(
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			if ($this->db->get('home_about')->num_rows() > 0 ){ $crud->unset_add(); }
			// ------------------- num_rows -----------------//


			$crud->set_field_upload('image','uploads/home_about');
			// ------------------- field_upload -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}


	public function about_category()
	{
			$crud = $this->is_function('about_category');
			$crud->set_subject('Category');
			$crud->columns(array('sort','image','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$display_as = array(
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image','uploads/about_category');
			// ------------------- field_upload -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}

	public function about_detail()
	{
			$crud = $this->is_function('about_detail');
			$crud->set_subject('Detail');
			$crud->columns(array('sort','image','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			if ($this->db->get('about_detail')->num_rows() > 0 ){ $crud->unset_add(); }
			// ------------------- num_rows -----------------//

			$display_as = array(
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image','uploads/about_detail');
			// ------------------- field_upload -----------------//
			
			$output = $crud->render();
			$this->_example_output($output);
	}
	
	public function about_ourteam()
	{
			$crud = $this->is_function('about_ourteam');
			$crud->set_subject('Our Team');
			$crud->columns(array('sort','image','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$display_as = array(
				'sort' => 'ลำดับ',
				'title_lang1' => 'ชื่อ',
				'title_lang2' => 'ตำแหน่ง',
				'subject_lang1' => 'เบอร์โทร',
				'subject_lang2' => 'อีเมล',
				'is_date' => 'วันที่/เวลา',
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image','uploads/about_ourteam');
			// ------------------- field_upload -----------------//
			
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function about_reference()
	{
			$crud = $this->is_function('about_reference');
			$crud->set_subject('Client Reference');
			$crud->columns(array('sort','image','is_date'));
			// ------------------- is_function -----------------//

			$display_as = array(
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image','uploads/about_reference');
			// ------------------- field_upload -----------------//
			
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function product_category()
	{
			$crud = $this->is_function('product_category');
			$crud->set_subject('Catgory');
			$crud->columns(array('sort','image','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$crud->where('tied', 0);
			$crud->field_type('tied', 'hidden');
			// ------------------- where -----------------//

			$display_as = array(
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image','uploads/product_category');
			// ------------------- field_upload -----------------//

			$crud->add_action('subcategory', '', 'admin/product_subcategory','fa fa-plus');
			
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function product_subcategory($id)
	{
			$crud = $this->is_function('product_category');
			$crud->set_subject('Catgory');
			$crud->columns(array('sort','image','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$crud->where('tied',$id);
			$crud->field_type('tied', 'hidden', $id);
			$crud->unset_columns('tied');
			// ------------------- where -----------------//

			$display_as = array(
				'image' => 'รูปภาพ (600x500)px',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image','uploads/product_category');
			// ------------------- field_upload -----------------//
			
			$output = $crud->render();
			$this->_example_output($output);
	}

	


	public function product()
	{
			$crud = $this->is_function('product');
			$crud->set_subject('Product');
			$crud->columns(array('category_id','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$category = array('');
			$this->db->order_by('sort', 'ASC');
			$this->db->where('tied', 0);
			$results = $this->db->get('product_category');
			foreach ($results->result() as $category0) { 
				$category[$category0->id] = $category0->title_lang1; 
				////////// level2 ////////
				$this->db->order_by('sort', 'ASC');
				$this->db->where('tied', $category0->id);
				$results1 = $this->db->get('product_category');
				foreach ($results1->result() as $category1) { 
					$category[$category1->id] = $category0->title_lang1.' --> '.$category1->title_lang1; 
				}
			}
			$crud->field_type('category_id', 'dropdown', $category);
			// ------------------- multiselect -----------------//

			$display_as = array(
				'category_id' => 'หมวดหมู่',
				'image1' => 'รูปภาพ1 (600x500)px',
				'image2' => 'รูปภาพ2 (600x500)px',
				'detail2_lang1' => 'เนื้อหาที่ 2(EN)',
				'detail2_lang2' => 'เนื้อหาที่ 2(TH)',	
				'detail3_lang1' => 'เนื้อหาที่ 3(EN)',
				'detail3_lang2' => 'เนื้อหาที่ 3(TH)',	
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image1','uploads/product');
			$crud->set_field_upload('image2','uploads/product');
			$crud->set_field_upload('file','uploads/product_file');
			// ------------------- field_upload -----------------//
			
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function news_category()
	{
			$crud = $this->is_function('news_category');
			$crud->set_subject('Catgory');
			$crud->columns(array('title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}

	public function news()
	{
			$crud = $this->is_function('news');
			$crud->set_subject('News');
			$crud->columns(array('category_id','title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			$crud->field_type('is_date', 'datetime');
			// ------------------- is_function -----------------//

			$category = array('');
			$results = $this->db->get('news_category');
			foreach ($results->result() as $category0) { $category[$category0->id] = $category0->title_lang1; }
			$crud->field_type('category_id', 'dropdown', $category);
			// ------------------- multiselect -----------------//

			$display_as = array(
				'category_id' => 'หมวดหมู่',
				'image1' => 'รูปภาพ1 (600x500)px',
				'image2' => 'รูปภาพ2 (600x500)px',
				'detail2_lang1' => 'เนื้อหาที่ 2(EN)',
				'detail2_lang2' => 'เนื้อหาที่ 2(TH)',	
				'detail3_lang1' => 'เนื้อหาที่ 3(EN)',
				'detail3_lang2' => 'เนื้อหาที่ 3(TH)',	
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->set_field_upload('image1','uploads/news');
			$crud->set_field_upload('image2','uploads/news');
			// ------------------- field_upload -----------------//
			
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function faq()
	{
			$crud = $this->is_function('faq');
			$crud->set_subject('Faq');
			$crud->columns(array('title_lang1','title_lang2','is_date'));
			$crud->required_fields('title_lang1','title_lang2');
			// ------------------- is_function -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}

	public function contact()
	{
			$crud = $this->is_function('contact');
			$crud->set_subject('Contact');
			$crud->columns(array('name','email','is_date'));
			// ------------------- is_function -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}

	public function contact_social()
	{
			$crud = $this->is_function('contact_social');
			$crud->set_subject('Contact Social');
			// $crud->columns(array('name','email','is_date'));
			$crud->required_fields('social_i','social_title');
			// ------------------- is_function -----------------//

			$display_as = array(
				'social_i' => 'ไอคอน',
				'social_title' => 'ชื่อ',
				'social_link' => 'ลิ้ง',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}

	public function contact_tel()
	{
			$crud = $this->is_function('contact_tel');
			$crud->set_subject('Contact Tel');
			// $crud->columns(array('name','email','is_date'));
			$crud->required_fields('title');
			// ------------------- is_function -----------------//

			$display_as = array(
				'title' => 'ชื่อ',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$crud->add_action('Detail', '', 'admin/contact_tel_datail','fa fa-plus');
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function contact_tel_datail($id)
	{
			$crud = $this->is_function('contact_tel_datail');
			$crud->set_subject('Contact Tel Deatil');
			// $crud->columns(array('name','email','is_date'));
			$crud->required_fields('icon','detail');
			// ------------------- is_function -----------------//

			$crud->field_type('tel_id', 'hidden');
			$crud->where('tel_id',$id);
			$crud->field_type('tel_id', 'hidden', $id);
			$crud->unset_columns('tel_id');
			// ------------------- where -----------------//

			$display_as = array(
				'icon' => 'ไอคอน',
				'detail' => 'รายละเอียด',
			);
			$crud->display_as($display_as);
			// ------------------- display_as -----------------//

			$output = $crud->render();
			$this->_example_output($output);
	}



	///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function home()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('bootstrap');
			$crud->set_subject('settings');
			$crud->set_table('settings');
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function settings()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('bootstrap');
			$crud->set_subject('settings');
			$crud->set_table('settings');
			$crud->set_field_upload('logo','uploads/admin');
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function users()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('bootstrap');
			$crud->set_subject('users');
			$crud->set_table('users');
			$crud->where('position', NULL);
			$crud->unset_columns('position','phone');
			$crud->set_relation_n_n('groups', 'users_groups', 'groups', 'user_id', 'group_id', 'name');
			$crud->field_type('password', 'password', NULL);
			$crud->field_type('position', 'hidden', NULL);
			$crud->callback_edit_field("password",array($this,"base64_decodes"));
			$crud->callback_before_insert(array($this,'encrypt_password'));
			$crud->callback_before_update(array($this,'encrypt_password'));
			$output = $crud->render();
			$this->_example_output($output);
	}

	function base64_decodes($value)
	{
		$post_array = '<input type="text" maxlength="50" value="" name="password" class="form-control">';
		return $post_array;
	}

	function encrypt_password($post_array, $primary_key = null)
	{
		$post_array['password'] = base64_encode($post_array['password'].'#TK@@!');
		return $post_array;
	}

	public function groups()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('bootstrap');
			$crud->set_subject('groups');
			$crud->set_table('groups');
			$output = $crud->render();
			$this->_example_output($output);
	}

	public function menu()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('bootstrap');
			$crud->set_subject('menu');
			$crud->set_table('menu');
			$crud->order_by('menu.sort', 'ASC'); 
			// $crud->add_action('Sub Menu', '', 'demo/action_more','fa fa-plus');
			$crud->add_action('Sub Menu', '', '','fa fa-plus',array($this,'link_sub_menu'));
			// $crud->field_type('sub_menu', 'hidden');
			// $crud->unset_columns('sub_menu');
			$crud->set_relation_n_n('Akses', 'groups_menu', 'groups', 'id_menu', 'id_groups', 'name');
			$output = $crud->render();
			$this->_example_output($output);
	}

	function link_sub_menu($primary_key, $row)
	{
		if ($row->url == "#") {
			$url = site_url('admin/sub_menu').'/'.$primary_key;
		}else{
			$url = "#";
		}
	    return $url;
	}

	public function sub_menu($id)
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('bootstrap');
			$crud->set_subject('menu_sub');
			$crud->set_table('menu_sub');
			$crud->order_by('menu_sub.sort', 'ASC'); 
			// $crud->add_action('Sub Menu', '', 'demo/action_more','fa fa-plus');
			// $crud->add_action('Photos', '', '','fa fa-plus',array($this,'link_sub_menu'));
			$crud->where('sub_menu',$id);
			$crud->field_type('sub_menu', 'hidden', $id);
			$crud->unset_columns('sub_menu');
			$crud->set_relation_n_n('Akses', 'groups_menu', 'groups', 'id_menu_sub', 'id_groups', 'name');
			$output = $crud->render();
			$this->_example_output($output);
	}

	///////// SEO /////////
	public function seo()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('bootstrap');
			$crud->set_subject('SEO');
			$crud->set_table('seo');
			/////////////////////
			$crud->callback_add_field('seo_keywords',array($this,'seo_callback'));
			$crud->callback_edit_field('seo_keywords',array($this,'seo_callback'));
			$crud->callback_add_field('seo_robots',array($this,'seo_radio'));
			$crud->callback_edit_field('seo_robots',array($this,'seo_radio'));
			$crud->callback_edit_field('seo_page', function ($value, $primary_key) {
				$url_return =  '<input type="text" maxlength="50" value="'.$value.'" name="seo_page" class="form-control disabled" disabled>';
				return $url_return;
			});
			//////////////////
			$crud->unset_add();
			$crud->unset_delete();
			$output = $crud->render();
			$this->_example_output($output);
	}

	function seo_callback($value)
	{
		return '<input id="tags" type="text" name="seo_keywords" class="form-control" value="'.$value.'" />';
	}

	function seo_radio($value)
	{
		if ($value==='on') {
			return '<input type="radio" name="seo_robots" value="index,follow" checked="checked"/> ON <input type="radio" name="seo_robots" value="noindex,nofollow"/> OFF';
		} else {
			return '<input type="radio" name="seo_robots" value="index,follow"/> ON <input type="radio" name="seo_robots" value="noindex,nofollow" checked="checked"/> OFF';
		}
	}
	

///////////////////////////////////////////////////
}