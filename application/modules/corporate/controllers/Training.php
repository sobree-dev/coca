<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Training extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('training_m');
	}

    private function seo()
	{
		$title          = "Corporate / Training";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
	}

	public function orientation()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => '',
            'header'  => 'header',
            'content' => 'training/orientation',
            'footer'  => 'footer',
            'function'=>  array('corporate'),
        );
        $data['get_all'] = $this->training_m->get_all();
        $this->load->view('template/body', $data);
    }
    
    public function orientation_form()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => '',
            'header'  => 'header',
            'content' => 'training/orientation_form',
            'footer'  => 'footer',
            'function'=>  array('corporate'),
        );
        $data['result'] = $this->training_m->get_by_id( base64_decode($this->uri->segment(4)) );
        $this->load->view('template/body', $data);
	}

	public function orientation_insert()
	{
		$input = $this->input->post();
        $this->training_m->insert($input);
        redirect( site_url('corporate/training/orientation'), 'refresh');
    }
    
    public function orientation_update()
	{
        $update_id = base64_decode($this->uri->segment(4));
        // update_id
        $input = $this->input->post();
        $this->training_m->update($update_id, $input);
        redirect( site_url('corporate/training/orientation'), 'refresh');
    }
    
    public function orientation_delete()
	{
        $update_id = base64_decode($this->uri->segment(4));
        // update_id
        $this->training_m->delete($update_id);
        redirect( site_url('corporate/training/orientation'), 'refresh');
	}
    
}