<?php

class DBrecord extends CI_Model {

    // protected $table = 'country';

    public function __construct() {
        parent::__construct();
    }

    public function get_result($thisTB) {
        $id = $thisTB['id'];
        $table = $thisTB['table'];
        $order_by = $thisTB['order_by'];
        if ($order_by['sort']=='asc') {
            $this->db->order_by('sort', 'asc');
        }
        if (!empty($id)) {
            $this->db->where($id);
        }
        $result = $this->db->get($table);
        return $result->result();
    }

    public function get_result2($thisTB) {
        $id = $thisTB['id'];
        $order_by = $thisTB['order_by'];
        $table = $thisTB['table'];
        if ($order_by['sort']=='asc') {
            $this->db->order_by('sort', 'asc');
        }
        if (!empty($id)) {
            $this->db->where('find_in_set('.$id.', branchID) <> 0');
        }
        $result = $this->db->get($table);
        return $result->result();
    }

    public function get_first($thisTB) {
        $id = $thisTB['id'];
        $table = $thisTB['table'];
        return $this->db->where($id)
                        ->get($table)
                        ->row();
    }

    public function insert($thisTB) {
        $value = $thisTB['value'];
        $table = $thisTB['table'];

        $this->db->insert($table, $value);
        return $this->db->insert_id();
    }
    
    public function update($thisTB)
    {
        $id = $thisTB['id'];
        $value = $thisTB['value'];
        $table = $thisTB['table'];

        $query = $this->db
                        ->where($id)
                        ->update($table, $value);
        return $query;
    }
    
    public function update_in($thisTB)
    {
        $id = $thisTB['id'];
        $value = $thisTB['value'];
        $table = $thisTB['table'];

        $query = $this->db
                        ->where_in($id)
                        ->update($table, $value);
        return $query;
    }

    public function delete($thisTB) {
        $id = $thisTB['id'];
        $table = $thisTB['table'];

        $query = $this->db
                        ->where($id)
                        ->delete($table);
        return $query;
    }

}